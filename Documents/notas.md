#Notas de proyecto

#SUGERENCIAS
1- Sugerencia: utilizar cap 8 de la guia para realizar una inicializacion del programa directo desde la memoria usb.

www.altera.com/literature/ug/ug_soc_eds.pdf. 

https://forums.intel.com/s/question/0D50P00003yyQftSAE/preloader-and-bare-metal-application-run?language=en_US


#PROBLEMAS ENFRENTADOS

1- Instalación de los ambientes de trabajo para el servidor y la PC.
    -> DS-5 Intel SoC FPGA Embedded Development Suite

2- Control de memoria desde el HPS
    -> Solo se puede cargar directamente a OC RAM (On chip)

3- Instalación de Eclipse (DS-5) Linux
    -> Instalación se queda pegada y no avanza ni cierra el wizard luego de mostrar "Installation complete"
        -Se soluciona cerrando el programa (brute force) con el comando xkill.
        -Luego buscar la ruta de instalación típicamente (<Quartus II Installation>/embedded/ds-5_installer)
        -Y ejecutar el script (install.sh) como root

    -> Se cae al intentar ejecutar el sw
        -Se soluciona cambiando la dirección de instalación a: <Quartus II Installation>/embedded/ds-5
        -Se corre el programa como root.

4- Error al intentar buscar la conexion usb blaster en (debug configurations)
    Error: Failed to load client library from <path to Quartus II>/quartus/linux/libjtag_client.so. Please ensure that the environment variable QUARTUS_ROOTDIR is set to the path to the Altera Quartus® tools installation

        -Se cambia el script para permitir el acceso a blaster usb
        -Se obtuvo información de:

        https://www.intel.com/content/www/us/en/programmable/support/support-resources/knowledge-base/solutions/rd06242013_219.html

    
#Lista de Software utilizado

1- Quartus Prime Lite 17.1 590 Build
Descarga: http://fpgasoftware.intel.com/17.1/?edition=lite

2- DS-5 Community Edition (Part of Intel SoC FPGA Embedded Development Suite) Standard Edition 17.1
Descarga: http://fpgasoftware.intel.com/soceds/17.1/?edition=standard&platform=linux&download_manager=dlm3