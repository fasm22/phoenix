module FiniteStateMachine (
	input clk,
	input reset,
	input [7:0]interrupt,
	input [7:0]readData,
	output [14:0] readMemPosition,
	output [14:0] writeMemPosition,
	output [7:0]writeData,
	output writeEnable,
	output [4:0]statusLED
);

localparam width = 64;
localparam height = 64;
localparam maxSize = width*height;

reg [4:0]currentState = 0;
reg [14:0] index = 0;//width + 1; //TODO: width + 1

reg [14:0] addressCounter = 0; //TODO: width + 1

reg [7:0] dataA;
reg [7:0] dataB;
reg [7:0] dataC;
reg [7:0] dataD;
reg [7:0] dataE;
reg [7:0] dataF;
reg [7:0] dataG;
reg [7:0] dataH;
reg [7:0] dataI;
wire [7:0] dataOut;

reg [7:0] regWriteData = 0;
reg regwriteEnable = 0;
reg startFilter = 0;

gaussian mod_gaussian0(
	.a(dataA),
	.b(dataB),
	.c(dataC),
	.d(dataD),
	.e(dataE),
	.f(dataF),
	.g(dataG),
	.h(dataH),
	.i(dataI),
	.out(dataOut)
);

/*
*	Wait for the 0x2B instruction to start the filter
*	application and change the startFilter value.
*/
always @(posedge clk) begin
	
	if(interrupt == 8'h2B && currentState < 1)begin
		startFilter = 1;
	end
	else if(interrupt != 8'h2B)begin
		startFilter = 0;
	end
	
end


always @(posedge clk) begin
	if(startFilter)begin
		case (currentState)
			5'd1 : begin
				addressCounter = index;
			end
			5'd4 : begin
				dataE = readData;
			end
			5'd10 : begin
				regWriteData = dataE;
				regwriteEnable = 0;
			end
			5'd14 : begin
				if(index >= maxSize) begin
					currentState = 5'b11111;
				end
				else begin
					regwriteEnable = 1;
					index = index + 1;
					currentState = 0;
				end
			end
		endcase
		currentState = currentState + 1;
	end//If(startFilter)
end

assign statusLED = currentState;
assign readMemPosition = addressCounter;
assign writeMemPosition = index;
assign writeData = regWriteData;
assign writeEnable = regwriteEnable;

endmodule 
 
