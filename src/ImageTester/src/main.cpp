#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h> 
#include <time.h>

using namespace cv;
using namespace std;

void saltAndPepperNoise(Mat & image, const double noiseProbability)
{
	//int imageChannels = image.channels();

//random_value_generator random;

	long noisePoints = noiseProbability * image.rows * image.cols * 1;

	for (long i = 0; i < noisePoints; i++)
	{
		int row = rand() % image.rows + 1;

		int column = rand() % image.cols + 1;

		int channel = 1;

		unsigned char * pixelValuePtr = image.ptr(row) + (column * 1) + channel;

		*pixelValuePtr = rand() % 1 + 0;
	}
}


/** @function main */
int main( int argc, char** argv )
{
    
  srand (time(NULL));
  Mat src, src_gray, dst;
  int kernel_size = 3;
  int scale = 2;
  int delta = 0;
  int ddepth = CV_16S;

  int c;

  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
    { return -1; }

  /// Remove noise by blurring with a Gaussian filter
  GaussianBlur( src, src, Size(3,3), 0, 0, BORDER_DEFAULT );

  /// Convert the image to grayscale
  cvtColor( src, src_gray, COLOR_BGR2GRAY );

  /// Create window
  namedWindow( "laplace demo", WINDOW_AUTOSIZE );

  /// Apply Laplace function
  Mat abs_dst;

  //Laplacian( src_gray, dst, ddepth, kernel_size, scale, delta, BORDER_DEFAULT );
  //convertScaleAbs( dst, abs_dst );
  
  /// Generate grad_x and grad_y
  Mat grad_x, grad_y;
  Mat abs_grad_x, abs_grad_y;
  Mat grad;

  /// Gradient X
  //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
  Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
  convertScaleAbs( grad_x, abs_grad_x );

  /// Gradient Y
  //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
  Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
  convertScaleAbs( grad_y, abs_grad_y );

  /// Total Gradient (approximate)
  addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );
  
  /// Show what you got
  imshow( "laplace demo", grad );
  
  
  
  imwrite("out.jpg", grad );

  waitKey(0);

  return 0;
} 
