#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include "../include/metrics.h"

using namespace cv;

int main( int argc, char** argv ){
    if( argc != 3){
     std::cout <<" Usage: ./ImageMetrics SWimage.jpg HWimage.jpg" << std::endl;
     return -1;
    }

    Mat softwareImage, hardwareImage;
    
    softwareImage = imread(argv[1], cv::IMREAD_GRAYSCALE);
    hardwareImage = imread(argv[2], cv::IMREAD_GRAYSCALE);

    double outputPSNR = getPSNR(softwareImage, hardwareImage);
    Scalar outputMSSIM = getMSSIM(softwareImage, hardwareImage);

    std::cout << "PSNR: " << outputPSNR << std::endl;
    std::cout << "MSSIM: " << outputMSSIM[0] << std::endl;


    /*if(! image.data ){
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", image );                   // Show our image inside it.

    waitKey(0);                                          // Wait for a keystroke in the window*/
    return 0;
}
