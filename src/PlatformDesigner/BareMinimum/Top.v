`timescale 1ns / 1ps
module Top(
   input		clk_clk,
   input		reset_reset_n,
	input 	buttonA,
	input		switchA,
   output	[6:0]segment0,
	output	[6:0]segment1,
	output	[6:0]segment2,
	output	[6:0]segment3,
	output	[6:0]segment4,
	output	[6:0]segment5
);

wire [23:0]w_memPositionCounter;

SegmentsControl mod_segment0(
	.clk(clk_clk),
	.reset(reset_reset_n),
	.number_in(w_memPositionCounter),
	.segment0(segment0),
	.segment1(segment1),
	.segment2(segment2),
	.segment3(segment3),
	.segment4(segment4),
	.segment5(segment5)
);

MemControl mod_memcontrol0(
	.clk(clk_clk),
   .reset(reset_reset_n),
	.buttonA(buttonA),
	.switchA(switchA),
   .memPosition(w_memPositionCounter)
);

endmodule 