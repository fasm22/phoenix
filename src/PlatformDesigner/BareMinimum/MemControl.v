module MemControl(
    input clk,//clock input
    input reset, // reset
	 input buttonA,
	 input switchA,
	 input inputMemData,
    output [23:0] memPosition,
	 output [23:0] displaySegments
);

reg [23:0] memData = 23'hAAFF;
reg [23:0] memCounter;
reg [23:0] currentSegmentOutput;
wire w_status_buttonA;

Debounce mod_buttonA(
	.clk(clk),
   .i_btn(buttonA),
   .o_state(w_status_buttonA),
   .o_ondn(),
   .o_onup()
);


always @(posedge w_status_buttonA) begin
	if (reset)
		memCounter <= 0;
	else
		memCounter <= memCounter + 1;
end

always @(*) begin
	if(switchA)
		currentSegmentOutput = memCounter;
	else
		currentSegmentOutput = inputMemData;
end

assign displaySegments = currentSegmentOutput;
assign memPosition = memCounter;

endmodule 