module SegmentsControl(
    input clk, 	//clock input
    input reset, 	//reset
	 input  [23:0]number_in,
    output [6:0]segment0,
	 output [6:0]segment1,
	 output [6:0]segment2,
	 output [6:0]segment3,
	 output [6:0]segment4,
	 output [6:0]segment5
 );
 

 SegmentShow mod_segment0(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[3:0]),
	.LED_out(segment0)
);

SegmentShow mod_segment1(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[7:4]),
	.LED_out(segment1)
);

SegmentShow mod_segment2(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[11:8]),
	.LED_out(segment2)
);

SegmentShow mod_segment3(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[15:12]),
	.LED_out(segment3)
);

SegmentShow mod_segment4(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[19:16]),
	.LED_out(segment4)
);

SegmentShow mod_segment5(
	.clk(clk),
	.reset(reset),
	.number_in(number_in[23:20]),
	.LED_out(segment5)
);
 
 endmodule 