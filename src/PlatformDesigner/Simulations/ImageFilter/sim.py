import os
import subprocess
import sys


'''
*   Executes a background command and waits for it to finish
*   @param pCommand: shell command to be executed
*   @param pExecPath: working directory to execute the command
*   @param saveToLogFileObject: composed object. [0] = 1 or 0 to save to external log file
*                                                [1] = path to save the external log file
'''
def runCommand(pCommand, pExecPath, saveToLogFileObject):

    if(saveToLogFileObject[0] == 1):
        fileOutput = open(saveToLogFileObject[1]+"/logfile.txt","w") 
        p = subprocess.Popen(pCommand, shell=True, stdout=fileOutput, stderr=fileOutput,cwd=pExecPath)
    else:
        p = subprocess.Popen(pCommand, shell=True, stdout=sys.stdout, stderr=sys.stderr,cwd=pExecPath)
    
    retval = p.wait()
    print("Process finished")
    
    if(saveToLogFileObject[0] == 1):
        fileOutput.close()

pModelsimDirectory = "/home/fasm22/intelFPGA/17.1/modelsim_ase/bin"
pExactRunPath = "/home/fasm22/workspace/ProjectGrad/phoenix/src/PlatformDesigner/Simulations/ImageFilter"
pExactSimulationFile = "/home/fasm22/workspace/ProjectGrad/phoenix/src/PlatformDesigner/Simulations/ImageFilter/simulation.do"

print("INFO: Running simulation")
command = "sh " + pModelsimDirectory + "/vsim" + " -c -do " + pExactSimulationFile
runCommand(command,pExactRunPath,[0,pExactRunPath])
