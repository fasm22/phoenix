`timescale 1ns / 1ps

module memModule();
  
  
  reg [7:0] memA [0:4096];
  
  integer i,j,file;

  integer pWidth = 64;
  integer pHeigth = 64;
  integer index = 0;

  reg [7:0] result;

  reg 	[7:0] dataA;
  reg 	[7:0] dataB;
  reg 	[7:0] dataC;
  reg 	[7:0] dataD;
  reg 	[7:0] dataE;
  reg 	[7:0] dataF;
  reg 	[7:0] dataG;
  reg 	[7:0] dataH;
  reg 	[7:0] dataI;
  wire 	[7:0] dataOut;

  gaussian mod_gaussian0(
    .a(dataA),
    .b(dataB),
    .c(dataC),
    .d(dataD),
    .e(dataE),
    .f(dataF),
    .g(dataG),
    .h(dataH),
    .i(dataI),
    .out(dataOut)
  );

  initial begin

    $readmemb("memA.dat", memA);
    file = $fopen("output.txt","w");
    
    #10
    
    /*for (i = 0; i < pHeigth; i = i + 1) begin
      for (j = 0; j < pWidth; j = j + 1) begin
        result = memA[index];
        #10
        $fwrite(file,"%d",result);
        if(j+1 < pWidth)
          $fwrite(file,",");
        index = index + 1;
      end
      $fwrite(file,"\n");
    end*/

    for(i = 0; i < pHeigth; i = i + 1)begin
        for(j = 0; j < pWidth; j = j + 1)begin
          dataA = memA[index - pWidth - 1];
          dataB = memA[index - pWidth];
          dataC = memA[index - pWidth + 1];
          dataD = memA[index - 1];
          dataE = memA[index];
          dataF = memA[index + 1];
          dataG = memA[index + pWidth - 1];
          dataH = memA[index + pWidth];
          dataI = memA[index + pWidth + 1];
          #10;
          if(j == 0)
            result = 0;
          else if(j < pWidth - 1)
            result = dataOut;
          else
            result = 0;

          $fwrite(file,"%d",result);
          if(j+1 < pWidth)
            $fwrite(file,",");
          index = index + 1;
        end
        $fwrite(file,"\n");
    end

    $fclose(file);
    
    $display("<< Simulation ended. >>");
    $finish;

  end
endmodule
