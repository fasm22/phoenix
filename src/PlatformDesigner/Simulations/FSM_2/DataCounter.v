module DataCounter (
	input clk,
	input reset,
	input [7:0] pData,
	input [4:0] pSelec,
	output [7:0] pResult
);

reg 	[7:0] dataA;
reg 	[7:0] dataB;
reg 	[7:0] dataC;
reg 	[7:0] dataD;
reg 	[7:0] dataE;
reg 	[7:0] dataF;
reg 	[7:0] dataG;
reg 	[7:0] dataH;
reg 	[7:0] dataI;
wire 	[7:0] dataOut;

gaussian mod_gaussian0(
	.a(dataA),
	.b(dataB),
	.c(dataC),
	.d(dataD),
	.e(dataE),
	.f(dataF),
	.g(dataG),
	.h(dataH),
	.i(dataI),
	.out(dataOut)
);

/*
*	Handles the references to the data A-I
*	to apply the filter kernel correctly.
*/
always @(*)begin
	if(~reset)begin
		dataA = 0;
		dataB = 0;
		dataC = 0;
		dataD = 0;
		dataE = 0;
		dataF = 0;
		dataG = 0;
		dataH = 0;
		dataI = 0;
	end
	else begin
		case(pSelec)
			5'd1 : dataA = pData;
			5'd2 : dataB = pData;
			5'd3 : dataC = pData;
			5'd4 : dataD = pData;
			5'd5 : dataE = pData;
			5'd6 : dataF = pData;
			5'd7 : dataG = pData;
			5'd8 : dataH = pData;
			5'd9 : dataI = pData;
		endcase
	end
end

assign pResult = dataOut;

endmodule 