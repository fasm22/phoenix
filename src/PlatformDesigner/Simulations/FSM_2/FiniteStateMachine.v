module Cloud (
	input clk,
	input reset,
	output [5:0]statusLED
);

/*
*	Paramters defined according to the size of
*	the image to be processed.
*/
localparam width = 64;
localparam height = 64;
localparam maxSize = width * height;

/*
*	Registers that handles the states of the FSM,
*	and the iterator status over the image.
*/
//REGISTERS
reg [4:0] currentState = 0;
reg [4:0] nextState = 1;
reg [14:0] addressCounter = 0;

reg clkIndexCounter = 0;
reg clkDataCounter = 0;
reg [7:0] pCurrentData = 0;

//WIRES
wire [14:0] index;

//MODULES
/*
*	Module that records the current index position
*	to retrieve the center of the image kernel.
*/
IndexCounter mod_IndexCounter1(
	.clk(clkIndexCounter),
	.reset(reset),
	.outputIndex(index)
);


DataCounter mod_DataCounter(
	.clk(clkDataCounter),
	.reset(reset),
	.pData(pCurrentData),
	.pSelec(currentState),
	.pResult()
);


/*
*	Block that updates the next state
*/
always @(posedge clk or negedge reset)begin
	if(~reset)begin
		currentState <= 5'd0;
	end
	else begin
		currentState <= nextState;
	end
end


/*
* 	Block that computes the next state and performs
*	the required operations on each state.
*/
always @(currentState)begin
	case (currentState)
        5'd0 : begin //Start module
			addressCounter = index;
			clkIndexCounter = 0;
			pCurrentData = 0;
            nextState = 1;
        end
		
		
		5'd1 : begin //Retrieve 'A' Kernel value
			addressCounter = index - width - 1;
			clkIndexCounter = 0;
			//pCurrentData = 134;
			pCurrentData = 132;
			nextState = 2;
		end
		5'd2 : begin //Retrieve 'B' Kernel value
			addressCounter = index - width;
			clkIndexCounter = 0;
			//pCurrentData = 132;
			pCurrentData = 132;
			nextState = 3;
		end
		5'd3 : begin //Retrieve 'C' Kernel value
			addressCounter = index - width + 1;
			clkIndexCounter = 0;
			//pCurrentData = 132;
			pCurrentData = 127;
			nextState = 4;
		end
		5'd4 : begin //Retrieve 'D' Kernel value
			addressCounter = index - 1;
			clkIndexCounter = 0;
			//pCurrentData = 135;
			pCurrentData = 126;
			nextState = 5;
		end
		5'd5 : begin //Retrieve 'E' Kernel value
			addressCounter = index;
			clkIndexCounter = 0;
			//pCurrentData = 126;
			pCurrentData = 128;
			nextState = 6;
		end
		5'd6 : begin //Retrieve 'F' Kernel value
			addressCounter = index + 1;
			clkIndexCounter = 0;
			//pCurrentData = 128;
			pCurrentData = 129;
			nextState = 7;
		end
		5'd7 : begin //Retrieve 'G' Kernel value
			addressCounter = index + width - 1;
			clkIndexCounter = 0;
			//pCurrentData = 131;
			pCurrentData = 130;
			nextState = 8;
		end
		5'd8 : begin //Retrieve 'H' Kernel value
			addressCounter = index + width;
			clkIndexCounter = 0;
			//pCurrentData = 130;
			pCurrentData = 128;
			nextState = 9;
		end
		5'd9 : begin //Retrieve 'I' Kernel value
			addressCounter = index + width + 1;
			clkIndexCounter = 0;
			//pCurrentData = 128;
			pCurrentData = 129;
			nextState = 10;
		end
				
		
		5'd10 : begin //Save value and compute next index
			if(index < maxSize)begin
				addressCounter = 0;
				clkIndexCounter = 1;
				nextState = 1;
			end
			else begin
				addressCounter = 0;
				clkIndexCounter = 1;
				nextState = 10;
			end
		end
        default : begin
			addressCounter = index;
			clkIndexCounter = 0;
			pCurrentData = 0;
            nextState = 1;
        end
	endcase
end

assign statusLED = nextState;

endmodule 