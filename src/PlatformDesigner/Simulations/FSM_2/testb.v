
`timescale 1ns / 1ps

module testbench();

reg rstn;
reg clock;

initial begin
  $display($time, " << Starting the Simulation >>");
    rstn = 1'b0;
    clock = 0;
    #5 rstn = 1'b1;
end

always #1 clock=~clock;

Cloud mod_cloud1(
	.clk(clock),
	.reset(rstn),
	.statusLED()
);


initial
begin
      $dumpfile("RC_Testbench.vcd");
      $dumpvars;
end

initial begin
    #100;
    $display("<< Simulation ended. >>");
    $finish;
end

endmodule