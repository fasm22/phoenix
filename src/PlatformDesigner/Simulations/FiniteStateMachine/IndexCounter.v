module IndexCounter (
	input clk,
	input reset,
	output [14:0]outputIndex
);

localparam width = 65;

reg [14:0] reg_OutputIndex = width;

/*
*	Handles the reference to the index iterator
*   over the pixels of the image on physical
*   memory.
*/
always @(posedge clk or negedge reset)begin
	if(~reset)begin
		reg_OutputIndex <= width;
	end
	else begin
		reg_OutputIndex <= reg_OutputIndex + 1;
	end
end

assign outputIndex = reg_OutputIndex;

endmodule 