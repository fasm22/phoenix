/*
 * \file RCA_15_0_tb.v 
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 */

`timescale 1ns / 1ps

module RC_Testbench();
  
  wire [7:0] s;
  reg [15:0] a;
  reg [15:0] b;
  
  reg [7:0] memA [0:9];
  
  integer i,file;

  //RCA_15_0 U0(a,b,s);
  
  gaussian modGauss(
		.a(memA[0]), 
		.b(memA[1]), 
		.c(memA[2]), 
		.d(memA[3]), 
		.e(memA[4]), 
		.f(memA[5]), 
		.g(memA[6]),
		.h(memA[7]), 
		.i(memA[8]), 
		.out(s)
);

  initial begin

    $display("-- Begining Simulation --");
    $readmemb("memA.dat", memA);
    file = $fopen("output.txt","w");
    
    #10
    
    $fwrite(file,"%d\n",s);
    
    /*for (i = 0; i < 10; i = i + 1) begin
      a = memA[i]+10;
      #10
      $fwrite(file,"%d\n",a);
    end*/

    $fclose(file);
    $display("-- Ending simulation --");
    $finish;
  end
endmodule
 
