# 1 Create a work directory for modelsim
vlib ./work

# 2 Compile files
vlog -work work testb.v
vlog -work work FiniteStateMachine.v
vlog -work work IndexCounter.v

# 3 Simulation
vsim testbench

# 4 Adding
#vcd file RC_Testbench.vcd
#vcd add *

# 5 Run simulation in testbench
run -all
vcd checkpoint

# 6 Quit Modelsim
quit
 
