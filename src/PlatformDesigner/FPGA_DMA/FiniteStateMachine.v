	module FiniteStateMachine (
	input clk,
	input reset,
	
	input [7:0] interruptInput,
	output [7:0] interruptOutput,
	output [7:0] interruptCounterOutput,
	
	input [7:0] readData,
	output [16:0] readMemPosition,
	
	output [16:0] writeMemPosition,
	output [7:0] writeData,
	output writeEnable,
	
	output [5:0]statusLED
);

///////////////// PARAMETERS ////////////////////////
localparam width = 128;
localparam height = 128;
localparam maxSize = width * height;

localparam maxCounter = 1024;

///////////////// REGISTERS ////////////////////////
reg [4:0] currentState = 0;
reg [4:0] nextState = 1;
reg clkIndexCounter = 0;

reg [16:0] addressCounter = 0;
reg regWriteEnable = 1;

//Memory Filter Registers
reg [7:0] pCurrentData;
reg [7:0] regInterruptOutput = 0;


///////////////// WIRES ////////////////////////

wire  [7:0] pResultData;
wire [16:0] index;
wire [7:0] wClockCounter;

///////////////// MODULES //////////////////////
/*
*	Module that records the current index position
*	to retrieve the center of the image kernel.
*/
IndexCounter mod_IndexCounter1(
	.clk(clkIndexCounter),
	.reset(reset),
	.outputIndex(index)
);

/*
*	Module that stores the values for the kernels values
*	A,B,C,D,E,F,G,H,I and then applies the filter.
*/
DataCounter mod_DataCounter(
	.reset(reset),
	.clock(clk),
	.pData(pCurrentData),
	.pSelec(currentState),
	.pResult(pResultData)
);


/*
*	Module designed to count the number of cycles used
*	by the FSM in the application of the filter.
*/

ClockCounter mod_ClockCounter(
	.reset(reset),
	.clk(clk),
	.pCurrentState(currentState),
	.pResult(wClockCounter)
);


/*
*	Block that updates the next state
*/
always @(posedge clk or negedge reset)begin
	if(!reset)begin
		currentState <= 5'd0;
	end
	else begin
		if(interruptInput == 8'h2B)
			currentState <= nextState;
	end
end


/*
* 	Block that identifies if the filter application has 
*	ended and send the end code '0xEF' to notify the HPS.
*/
always @(currentState)begin
	case (currentState)
		5'd0 : regInterruptOutput = 0;
		5'd1 : regInterruptOutput = 0;
		5'd2 : regInterruptOutput = 0;
		5'd3 : regInterruptOutput = 0;
		5'd4 : regInterruptOutput = 0;
		5'd5 : regInterruptOutput = 0;
		5'd6 : regInterruptOutput = 0;
		5'd7 : regInterruptOutput = 0;
		5'd8 : regInterruptOutput = 0;
		5'd9 : regInterruptOutput = 0;
		5'd10 : regInterruptOutput = 0;
		5'd11 : regInterruptOutput = 8'hEF;
		default : regInterruptOutput = 0;
	endcase
end

/*
* 	Block that computes the next state and performs
*	the required operations on each state.
*/
always @(currentState)begin
	case (currentState)
		5'd0 : begin //Start module
			addressCounter = index;
			clkIndexCounter = 0;
			pCurrentData = 0;
			regWriteEnable = 1;
			nextState = 1;
		end
		
		
		5'd1 : begin //Retrieve 'A' Kernel value
			addressCounter = index - width - 1;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 2;
		end
		5'd2 : begin //Retrieve 'B' Kernel value
			addressCounter = index - width;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 3; 
		end
		5'd3 : begin //Retrieve 'C' Kernel value
			addressCounter = index - width + 1;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 4;
		end
		5'd4 : begin //Retrieve 'D' Kernel value
			addressCounter = index - 1;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 5;
		end
		5'd5 : begin //Retrieve 'E' Kernel value
			addressCounter = index;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 6;
		end
		5'd6 : begin //Retrieve 'F' Kernel value
			addressCounter = index + 1;
			clkIndexCounter = 0;
			regWriteEnable = 1;
			pCurrentData = readData;
			nextState = 7; 
		end
		5'd7 : begin //Retrieve 'G' Kernel value
			addressCounter = index + width - 1;
			clkIndexCounter = 0;
			regWriteEnable = 1; 
			pCurrentData = readData;
			nextState = 8;
		end
		5'd8 : begin //Retrieve 'H' Kernel value
			addressCounter = index + width;
			clkIndexCounter = 0;
			pCurrentData = readData;
			regWriteEnable = 1; 
			nextState = 9;
		end
		5'd9 : begin //Retrieve 'I' Kernel value
			addressCounter = index + width + 1;
			clkIndexCounter = 0;
			pCurrentData = readData;
			regWriteEnable = 0; //OJO. Para avMode = 1. regWriteEnable = 1. //TODO. TEST FOR EVERYTHING ELSE
			nextState = 10;
		end
		
		
		5'd10 : begin //Save value and compute next index
			if(index < maxSize)begin
				regWriteEnable = 1;
				addressCounter = 0;
				clkIndexCounter = 1;
				pCurrentData = 0;
				nextState = 1;
			end
			else begin
				regWriteEnable = 1;
				addressCounter = 0;
				clkIndexCounter = 1;
				pCurrentData = 0;
				nextState = 11;
			end
		end
		
		5'd11 : begin
			regWriteEnable = 1;
			addressCounter = 0;
			clkIndexCounter = 1;
			pCurrentData = 0;
			nextState = 11;
		end
		
		default : begin
			regWriteEnable = 1;
			addressCounter = index;
			clkIndexCounter = 0;
			pCurrentData = 0;
			nextState = 1;
		end
	endcase
end

assign readMemPosition = addressCounter;	//Memory position to read from
assign writeMemPosition = index;				//Memory position to write to
assign writeData = pResultData;				//Data to be written
assign writeEnable = regWriteEnable;		//Signal to enable write operation

assign interruptOutput = regInterruptOutput;
assign interruptCounterOutput = wClockCounter;

assign statusLED = nextState;

endmodule
