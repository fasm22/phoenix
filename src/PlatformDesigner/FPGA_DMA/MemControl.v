module MemControl(
    input clk,//clock input
    input reset, // reset
	 input buttonA,
	 input buttonB,
	 input buttonC,
	 input buttonD,
	 input switchA,
	 input chipSelect,
	 input [7:0]inputMemDataA,
	 input [7:0]inputMemDataB,
    output [14:0] memPosition,
	 output [23:0] displaySegments
);

reg [14:0] memCounter;
reg [23:0] currentSegmentOutput;
wire w_status_buttonA;
wire w_status_buttonB;
wire w_status_buttonC;
wire w_status_buttonD;

Debounce mod_buttonA(
	.clk(clk),
   .i_btn(buttonA),
   .o_state(),
   .o_ondn(w_status_buttonA),
   .o_onup()
);

Debounce mod_buttonB(
	.clk(clk),
   .i_btn(buttonB),
   .o_state(),
   .o_ondn(w_status_buttonB),
   .o_onup()
);

Debounce mod_buttonC(
	.clk(clk),
   .i_btn(buttonC),
   .o_state(),
   .o_ondn(w_status_buttonC),
   .o_onup()
);

Debounce mod_buttonD(
	.clk(clk),
   .i_btn(buttonD),
   .o_state(),
   .o_ondn(w_status_buttonD),
   .o_onup()
);

always @(posedge clk) begin
	if (reset)
		memCounter <= 0;
	else
		if (w_status_buttonA)
			memCounter <= memCounter + 1;
		else if (w_status_buttonB)
			memCounter <= memCounter - 1;
		else if (w_status_buttonC)
			memCounter <= memCounter + 256;
		else if (w_status_buttonD)
			memCounter <= memCounter - 256;
end

always @(*) begin
	if(switchA)
		currentSegmentOutput = memCounter;
	else begin
		if(chipSelect)
			currentSegmentOutput = inputMemDataA;
		else
			currentSegmentOutput = inputMemDataB;
	end
end

assign displaySegments = currentSegmentOutput;
assign memPosition = memCounter;

endmodule 