module Cloud (
	input clk,
	input reset,
	input [7:0]interrupt,
	input [7:0]readData,
	output [14:0] readMemPosition,
	output [14:0] writeMemPosition,
	output [7:0]writeData,
	output writeEnable,
	output [5:0]statusLED,
	output [3:0]outLED
);

localparam width = 64;
localparam height = 64;
localparam maxSize = width*height;

reg [4:0]currentState = 0;
reg [4:0]nextState = 5'd1;

reg [14:0] index = 0;
reg [14:0] addressCounter = 0;

reg [7:0] regWriteData = 0;
reg regwriteEnable = 0;

reg isRunning = 1;


/*
*	Block that updates the next state
*/
always @(posedge clk or posedge reset)begin
	if(reset)begin
		currentState <= 5'd0;
	end
	else begin
		currentState <= nextState;
	end
end


/*
* 	Block that computes the next state and performs
*	the required operations on each state.
*/
always @(currentState)begin
	case (currentState)
		5'd1 : begin
			index = 5;
			nextState = 2;
		end
		5'd2 : begin
			index = 10;
			nextState = 3;
		end
		5'd3 : begin
			index = 8;
			nextState = 4;
		end
		5'd4 : begin
			index = 12;
			nextState = 5;
		end
		5'd5 : begin
			index = 14;
			isRunning = 0;
		end
	endcase
end


assign readMemPosition = addressCounter;
assign writeMemPosition = index;
assign writeData = regWriteData;
assign writeEnable = regwriteEnable;

assign statusLED = nextState;
assign outLED = index;

endmodule 