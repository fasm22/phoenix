module InterruptControl(
    input clk,//clock input
    input reset, // reset
	 input buttonA,
	 
	 input [7:0] interruptCode,
	 input [7:0] interruptCounter,
	 
    output [7:0] memPositionA,
	 output [7:0] memWriteDataA,
	 output writeEnableA,
	 

	 output [7:0] memWriteDataB,
	 output [7:0] memWriteDataC

);


reg [5:0] memCounter = 0;
reg [7:0] memData = 8'h22;
reg memWriteEnable = 0;

wire w_status_buttonA;

Debounce mod_buttonA(
	.clk(clk),
   .i_btn(buttonA),
   .o_state(),
   .o_ondn(w_status_buttonA),
   .o_onup()
);


always @(posedge clk) begin
	if(w_status_buttonA)begin
		memData <= memData + 1;
		memWriteEnable = 1;
	end
	else
		memWriteEnable = 0;
end

assign memPositionA = memCounter;
assign memWriteDataA = memData;
assign writeEnableA = memWriteEnable;

assign memWriteDataB = interruptCode;
assign memWriteDataC = interruptCounter;

endmodule 