module Mux(
    input clk,
    input reset,
	 input select,
	 input 	[14:0]in1,
	 input 	[14:0]in2,
	 output 	[14:0]out
);

reg [14:0] outData;

always @(posedge clk) begin
	if(select)
		outData <= in2;
	else
		outData <= in1;
end

assign out = outData;

endmodule 