module Predictor (
	input reset,
	input clock,
	input [7:0] pDataA,
	input [7:0] pDataB,
	input [7:0] pDataC,
	input [7:0] pDataD,
	input [7:0] pDataE,
	input [7:0] pDataF,
	input [7:0] pDataG,
	input [7:0] pDataH,
	input [7:0] pDataI,
	input [7:0] pResultExact,
	input [7:0] pResultApprox,
	output [7:0] result
);

localparam c1 = 21;
localparam c2 = 6;
localparam c3 = 14;
localparam c4 = 25;
localparam c5 = 18;
localparam c6 = 31;
localparam c7 = 16;
localparam c8 = 44;
localparam c9 = 35;
localparam interr = 5;
localparam classDivision = 800;

reg[31:0] localResult = 0;
reg[31:0] finalResult = 0;

always @(posedge clock)begin
	if(localResult > classDivision)
		finalResult = pResultExact;
	else
		finalResult = pResultApprox;
	
end

always @(*)begin
	localResult = (interr + c1*pDataA + c2*pDataB + c3*pDataC +
					  c4*pDataD + c5*pDataE + c6*pDataF +
					  c7*pDataG + c8*pDataH + c9*pDataI) >> 5;
					  
	//localResult = localResult >> 5;
end

assign result = finalResult;

endmodule 