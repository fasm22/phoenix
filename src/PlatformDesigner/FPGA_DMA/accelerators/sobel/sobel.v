/*
 * \file sobel.v 
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Accurate Sobel filter implementation
 *
 */

module sobel (
		input [7:0] a, 
		input [7:0] b, 
		input [7:0] c, 
		input [7:0] d, 
		input [7:0] f, 
		input [7:0] g,
		input [7:0] h, 
		input [7:0] i, 
		output [7:0] out
);

	wire [8:0] x1, x2, x4, x5, y1, y2, y4, y5;
	wire [9:0] x3, x6, y3, y6;
  wire [10:0] gx, gy, absGx, absGy, filter, out1;

  // X-kernel

  //int x1 = a + g; //8 -> 9
  RCA_N8 R0 (a[7:0], g[7:0], x1[8:0]);
	//int x2 = d * 2; //8 -> 9
  assign x2[8:0] = {d[7:0],1'b0};
  //int x3 = x1 + x2; //9 -> 10
  RCA_N9 R1 (x1[8:0], x2[8:0], x3[9:0]);

  //int x4 = c + i; //8 -> 9
  RCA_N8 R2 (c[7:0], i[7:0], x4[8:0]);
	//int x5 = f * 2; //8 -> 9
  assign x5[8:0] = {f[7:0],1'b0};
  //int x6 = x4 + x5; //9 -> 10
  RCA_N9 R3 (x4[8:0], x5[8:0], x6[9:0]);

  //int gx = x6 - x3; //10 -> 11
  assign gx[10:0] = x6[9:0] - x3[9:0];

  //int absGx = abs(gx);
  assign absGx = gx[10] ? -gx : gx; 


  // Y-kernel

  //int y1 = a + c; //8 -> 9
  RCA_N8 R4 (a[7:0], c[7:0], y1[8:0]);
	//int y2 = b * 2; //8 -> 9
  assign y2[8:0] = {b[7:0],1'b0};
  //int y3 = y1 + y2; //9 -> 10
  RCA_N9 R5 (y1[8:0], y2[8:0], y3[9:0]);

  //int y4 = g + i; //8 -> 9
  RCA_N8 R6 (g[7:0], i[7:0], y4[8:0]);
	//int y5 = h * 2; //8 -> 9
  assign y5[8:0] = {h[7:0],1'b0};
  //int y6 = y4 + y5; //9 -> 10
  RCA_N9 R7 (y4[8:0], y5[8:0], y6[9:0]);

  //int gy = y3 - y6; //10 -> 11
  assign gy[10:0] = y3[9:0] - y6[9:0];

  //int absGy = abs(gy);
  assign absGy = gy[10] ? -gy : gy; 


  //int filter = absGx + absGy;
  //assign filter[10:0] = absGx[10:0] + absGy[10:0];
  RCA_N10 R8 (absGx[9:0], absGy[9:0], filter[10:0]);

  // Limit output to 255 or 0 (threshold)
  //assign out1[10:0] = (filter[10:0] < 10'h0ff) ? filter[7:0] : 8'hff;
  assign out1[10:0] = (filter[10:0] < 10'h000) ? (10'h000) : (filter[10:0]);
  assign out[7:0] = (out1[10:0] < 10'h0ff) ? (out1[7:0]) : (8'hff);

endmodule
