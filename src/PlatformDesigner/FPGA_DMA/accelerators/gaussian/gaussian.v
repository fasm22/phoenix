/*
 * \file gaussian.v 
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Accurate Gaussian filter implementation
 *
 */

module gaussian (
		input [7:0] a,
		input [7:0] b,
		input [7:0] c,
		input [7:0] d,
		input [7:0] e,
		input [7:0] f,
		input [7:0] g,
		input [7:0] h,
		input [7:0] i,
		output [7:0] out
);

  wire [8:0] b2, d2, f2, h2;
  wire [9:0] e4, s1, s2, s4, res;
  wire [10:0] s3, s5;
  wire [11:0] s6;
  wire [12:0] s7;
  wire [13:0] s8;

  //int b2 = b * 2; //8 -> 9
  assign b2[8:0] = {b[7:0],1'b0};
	//int d2 = d * 2; //8 -> 9
  assign d2[8:0] = {d[7:0],1'b0};
  //int e4 = e * 4; //8 -> 10
  assign e4[9:0] = {e[7:0],2'b00}; 
	//int f2 = f * 2; //8 -> 9
  assign f2[8:0] = {f[7:0],1'b0};
	//int h2 = h * 2; //8 -> 9
  assign h2[8:0] = {h[7:0],1'b0};

  //Op: int s1 = a + b2; //9 -> 10
  RCA_N9 S1 ({1'b0,a[7:0]},b2[8:0],s1[9:0]);
  //Op: int s2 = c + d2; //9 -> 10
  RCA_N9 S2 ({1'b0,c[7:0]},d2[8:0],s2[9:0]);
  //Op: int s3 = e4 + f2; //10 -> 11
  RCA_N10 S3 (e4[9:0],{1'b0,f2[8:0]},s3[10:0]);
  //Op: int s4 = g + h2; //9 -> 10
  RCA_N9 S4 ({1'b0,g[7:0]},h2[8:0],s4[9:0]);

  //Op: int s5 = s1 + s2; //10 -> 11
  RCA_N10 S5 (s1[9:0],s2[9:0],s5[10:0]);
  //Op: int s6 = s3 + s4; //11 -> 12
  RCA_N11 S6 (s3[10:0],{1'b0,s4[9:0]},s6[11:0]);
  
  //Op: int s7 = s5 + s6; //12 -> 13
  RCA_N12 S7 ({1'b0,s5[10:0]}, s6[11:0], s7[12:0]);
  
  //Op: int s8 = s7 + i; //13 -> 14
  RCA_N13 S8 (s7[12:0], {5'h00,i[7:0]}, s8[13:0]);
  
  //Op: int res = s8 >> 4;					// Original
  assign res[9:0] = s8[13:4];				//	Operation

  //Simplest approximation (Don't add i)
  //assign res[9:0] = s7[12:4]; //12:5
  
  assign out[7:0] = (res[9:0] < 10'h0ff) ? (res[7:0]) : (8'hff);
  
endmodule
