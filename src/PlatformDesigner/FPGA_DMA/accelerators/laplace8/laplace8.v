/*
 * \file laplace8.v 
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Accurate Gaussian filter implementation
 *
 */

module laplace8 (
		input [7:0] a, 
		input [7:0] b, 
		input [7:0] c, 
		input [7:0] d, 
		input [7:0] e, 
		input [7:0] f, 
		input [7:0] g,
		input [7:0] h, 
		input [7:0] i, 
		output [7:0] out
	);

	wire [8:0] s1, s2, s3, s4;
	wire [9:0] s5, s6;
  wire [10:0] s7, e8, res, out1;

  //int s1 = a + b; //8 -> 9
	RCA_N8 R0 (a[7:0], b[7:0], s1[8:0]);
	//int s2 = c + d; //8 -> 9
	RCA_N8 R1 (c[7:0], d[7:0], s2[8:0]);
  //int s3 = f + g; //8 -> 9
	RCA_N8 R2 (f[7:0], g[7:0], s3[8:0]);
	//int s4 = h + i; //8 -> 9
	RCA_N8 R3 (h[7:0], i[7:0], s4[8:0]);

  //int s5 = s1 + s2; //9 -> 10
	RCA_N9 R4 (s1[8:0], s2[8:0], s5[9:0]);
  //int s6 = s3 + s4; //9 -> 10
	RCA_N9 R5 (s3[8:0], s4[8:0], s6[9:0]);

  //int s7 = s5 + s6; //10 -> 11
	RCA_N10 R6 (s5[9:0], s6[9:0], s7[10:0]);

  //int e8 = e * 8; //8 -> 11
  assign e8[10:0] = {e[7:0],2'b000}; 

	//int res = e8 - s7; //11 -> 11
	assign res[10:0] = e8[10:0] - s7[10:0];
	
  assign out1[10:0] = (res[10:0] < 10'h000) ? (10'h000) : (res[10:0]);
  assign out[7:0] = (out1[10:0] < 10'h0ff) ? (out1[7:0]) : (8'hff);
endmodule
