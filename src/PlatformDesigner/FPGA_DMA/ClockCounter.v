module ClockCounter (
	input reset,
	input clk,
	input [4:0] pCurrentState,
	output [7:0] pResult
);


localparam maxCounter = 1024;

reg [15:0] regTempCounter = 0;
reg [7:0] regInterruptCounter = 1;

always @(posedge clk or negedge reset)begin
	if(!reset)begin
		regTempCounter <= 0;
	end
	else begin
		if(pCurrentState > 0 && pCurrentState < 11)begin
			if(regTempCounter < maxCounter-1)begin
				regTempCounter <= regTempCounter + 1;
			end
			else begin
				if(regInterruptCounter < 255)
					regInterruptCounter <= regInterruptCounter + 1;
				
				regTempCounter <= 0;
			end
		end
	end
end

assign pResult = regInterruptCounter;

endmodule 