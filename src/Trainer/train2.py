from pandas import DataFrame
from sklearn import linear_model
import statsmodels.api as sm
import matplotlib.pyplot as plt
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline

from sklearn.linear_model import LogisticRegressionCV


trainFile = open("Simulations/outputTrainData.csv", "r")

dataA = []
dataB = []
dataC = []
dataD = []
dataE = []
dataF = []
dataG = []
dataH = []
dataI = []
dataDiff = []
dataResult = []

for line in trainFile:
    dataInfo = line.split(",")[:-1]
    
    dataA.append(int(dataInfo[0]))
    dataB.append(int(dataInfo[1]))
    dataC.append(int(dataInfo[2]))
    dataD.append(int(dataInfo[3]))
    dataE.append(int(dataInfo[4]))
    dataF.append(int(dataInfo[5]))
    dataG.append(int(dataInfo[6]))
    dataH.append(int(dataInfo[7]))
    dataI.append(int(dataInfo[8]))

    dataDiff.append(int(dataInfo[10]))
    dataResult.append(int(dataInfo[11]))

trainFile.close()

TestData = {'VarA': dataA,
            'VarB': dataB,
            'VarC': dataC,
            'VarD': dataD,
            'VarE': dataE,
            'VarF': dataF,
            'VarG': dataG,
            'VarH': dataH,
            'VarI': dataI,
            'Diff': dataDiff,
            'Res' : dataResult}

#Load dataset
ds = DataFrame(TestData,columns=['VarA','VarB','VarC','VarD','VarE','VarF','VarG','VarH','VarI','Diff','Res'])

#Load X,Y values
X = ds[['VarA','VarB','VarC','VarD','VarE','VarF','VarG','VarH','VarI']]

#X_PCA = PCA(n_components=8,svd_solver='full')
#X_PCA.fit(X)

#print("Ratio:" + str(X_PCA.explained_variance_ratio_))
#print("SV: " + str(X_PCA.singular_values_))

#print(np.cumsum(np.round(X_PCA.explained_variance_ratio_, decimals=4)*100))

Y = ds['Res']

#Principle components regression
#steps = [
#    ('pca', PCA()),
#    ('estimator', LinearRegression())
#]
#pipe = Pipeline(steps)
#pca = pipe.set_params(pca__n_components=9,estimator__solver='lbfgs',estimator__max_iter=500)
#pca.fit(X, Y)

#print('Intercept: \n', pca.named_steps['estimator'].intercept_)
#print('Coeff: \n', pca.named_steps['estimator'].coef_)

#pca = LogisticRegression(random_state=1, solver='lbfgs',multi_class='ovr',tol=1)
pca = LinearRegression()
pca.fit(X,Y)

print('Intercept: \n', pca.intercept_)
print('Coeff: \n', pca.coef_)

#scaler = StandardScaler()
#scaledCoeff = scaler.fit_transform(pca.coef_)
#print('Coeff Normalized: \n',scaledCoeff)


#Train model with Linear Regression
#regr = linear_model.LinearRegression()
#regr.fit(X, Y)

#Show results 
#print('Intercept: \n', regr.intercept_)
#print('Coefficients: \n', regr.coef_)

                  #TP,FP,FN,TN
confusionMatrix = [0,0,0,0]

print( pca.predict([[131, 129, 126, 131, 129, 126, 131, 130, 125]])[0] )
print( pca.predict([[25, 26, 30, 26, 28, 30, 25, 29, 32]])[0] )
print( pca.predict([[206, 204, 203, 207, 205, 204, 208, 204, 200]])[0] )
print( pca.predict([[36, 59, 46, 41, 50, 51, 47, 49, 44]])[0] )

print( pca.predict([[22, 42, 68, 22, 43, 65, 21, 34, 58]])[0] )
print( pca.predict([[152, 135, 101, 154, 136, 101, 154, 137, 102]])[0] )
print( pca.predict([[82, 83, 89, 78, 88, 95, 80, 92, 96]])[0] )
print( pca.predict([[154, 136, 118, 155, 140, 109, 158, 142, 116]])[0] )

iter = 0
#for line in trainFile:
for i in range(0,12000):
    resPredict = pca.predict([[ dataA[iter],dataB[iter],dataC[iter],
                                dataD[iter],dataE[iter],dataF[iter],
                                dataG[iter],dataH[iter],dataI[iter] ]])[0]
    if(resPredict < 0.52):
        resPredict = 1
    else:
        resPredict = 0

    resReal = dataResult[iter]
    if(resPredict == 1):
        if(resReal == 1):
            confusionMatrix[0]+=1
        else:
            confusionMatrix[1]+=1
    elif(resPredict == 0):
        if(resReal == 1):
            confusionMatrix[2]+=1
        else:
            confusionMatrix[3]+=1

    iter+=1

print("Confusion Matrix: ", confusionMatrix)



#plt.scatter(ds['VarA'], ds['Diff'], color='red')
#plt.title('Data vs Difference', fontsize=14)
#plt.xlabel('Data C', fontsize=14)
#plt.ylabel('Difference', fontsize=14)
#plt.grid(True)
#plt.show()