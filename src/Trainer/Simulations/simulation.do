# 1 Create a work directory for modelsim
vlib ./work

# 2 Compile files
vlog -work work testb.v
vlog -work work memModule.v
vlog -work work fullAdder.v
vlog -work work gaussian.v
vlog -work work RCA_15_0.v
vlog -work work RCA_N9.v 
vlog -work work RCA_N10.v 
vlog -work work RCA_N11.v 
vlog -work work RCA_N12.v 
vlog -work work RCA_N13.v

# 3 Simulation
vsim testbench

# 4 Adding
#vcd file RC_Testbench.vcd
#vcd add *

# 5 Run simulation in testbench
run -all
vcd checkpoint

# 6 Quit Modelsim
quit
 
