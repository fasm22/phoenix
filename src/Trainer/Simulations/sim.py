import os
import subprocess
import sys


'''
*   Executes a background command and waits for it to finish
*   @param pCommand: shell command to be executed
*   @param pExecPath: working directory to execute the command
*   @param saveToLogFileObject: composed object. [0] = 1 or 0 to save to external log file
*                                                [1] = path to save the external log file
'''
def runCommand(pCommand, pExecPath, saveToLogFileObject):

    if(saveToLogFileObject[0] == 1):
        fileOutput = open(saveToLogFileObject[1]+"/logfile.txt","w") 
        p = subprocess.Popen(pCommand, shell=True, stdout=fileOutput, stderr=fileOutput,cwd=pExecPath)
    else:
        p = subprocess.Popen(pCommand, shell=True, stdout=sys.stdout, stderr=sys.stderr,cwd=pExecPath)
    
    retval = p.wait()
    print("Process finished")
    
    if(saveToLogFileObject[0] == 1):
        fileOutput.close()

'''
*   Parses the array type object to a string a removes the
*   '[' and ']' symbols to the string.
*   @param pArray: array object that contains the data
*   @param pFile: reference to the file that is being written
'''
def writeLineToFile(pArray,pFile):
    lineToWrite = str(pArray)
    lineToWrite=lineToWrite[1:]
    lineToWrite=lineToWrite[:-1]
    pFile.write(lineToWrite + ",\n")

'''
*   Generates the file to train the model by reading the files
*   created by the simulation script.
*   @param pWidth: Width value of the image that is being processed.
*   @param pHeight: Height value of the image that is being processed.
*   @param allowError: Max absolute error size for the values.
'''
def generateTrainData(pWidth, pHeight, allowError):
    maxSize = pWidth * pHeight
    
    print("INFO: Generating training data...")
    
    fileDataExact = open("Simulations/trainDataExact.txt", "r")
    fileDataApprox = open("Simulations/trainDataApprox.txt", "r")  

    fileOutput = open("Simulations/outputTrainData.csv", "w")  
    
    markedAsNonError = 0
    markedAsError = 0

    for line in range(0,maxSize): 
        exactValues = fileDataExact.readline().split(",")[:-1]
        approxValues = fileDataApprox.readline().split(",")[:-1]
        currentLine = []

        

        if('  x' in exactValues or 'i' in exactValues) :
            pass
        else:
            for number in exactValues:
                currentLine.append(int(number))

            diff = abs(int(exactValues[-1])-int(approxValues[-1]))
            
            currentLine.append(diff)

            if(diff >= allowError):
                currentLine.append(1)   #Error found, mark as 1
                markedAsError+=1
            else:
                currentLine.append(0)   #No error, mark as 0
                markedAsNonError+=1

            #print(currentLine)
            writeLineToFile(currentLine,fileOutput)

    print("Elements without error: " + str(markedAsNonError))
    print("Elements with error: " + str(markedAsError))

'''
*   Execute the script process
'''

pModelsimDirectory = "/home/fasm22/intelFPGA/17.1/modelsim_ase/bin"
pExactRunPath = "/home/fasm22/workspace/ProjectGrad/phoenix/src/Trainer/Simulations"
pExactSimulationFile = "/home/fasm22/workspace/ProjectGrad/phoenix/src/Trainer/Simulations/simulation.do"

print("INFO: Running simulation")
command = "sh " + pModelsimDirectory + "/vsim" + " -c -do " + pExactSimulationFile
runCommand(command,pExactRunPath,[0,pExactRunPath])
generateTrainData(128,128,2)

