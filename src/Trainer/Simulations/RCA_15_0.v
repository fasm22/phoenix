/*
 * \file RCA_15_0.v 
 * \author Jorge Castro-Godinez <jorge.castro-godinez@kit.edu>
 * Chair for Embedded Systems (CES)
 * Karlsruhe Institute of Technology (KIT), Germany
 * Prof. Dr. Joerg Henkel
 *
 * \brief Verilog implementation of a 16-bit Ripple Carry Adder (RCA)
 *
 */

module fullAdder (
		input a, 
		input b,
		input ci,
		output s,
		output co
	);

	wire p,g;

	// propagation signal
	xor (p,a,b);
	// generation signal
	and (g,a,b);

	// sum calculation
	assign s = p ^ ci;
	//carry output array calculation
	assign co = g | (p & ci);

endmodule


module RCA_15_0(
		input [15:0] in1, 
		input [15:0] in2,
		output [16:0] out
);

	wire c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15;

	fullAdder FA0(in1[0],in2[0],1'b0,out[0],c0);
	fullAdder FA1(in1[1],in2[1],c0,out[1],c1);
	fullAdder FA2(in1[2],in2[2],c1,out[2],c2);
	fullAdder FA3(in1[3],in2[3],c2,out[3],c3);
	fullAdder FA4(in1[4],in2[4],c3,out[4],c4);
	fullAdder FA5(in1[5],in2[5],c4,out[5],c5);
	fullAdder FA6(in1[6],in2[6],c5,out[6],c6);
	fullAdder FA7(in1[7],in2[7],c6,out[7],c7);
	fullAdder FA8(in1[8],in2[8],c7,out[8],c8);
	fullAdder FA9(in1[9],in2[9],c8,out[9],c9);
	fullAdder FA10(in1[10],in2[10],c9,out[10],c10);
	fullAdder FA11(in1[11],in2[11],c10,out[11],c11);
	fullAdder FA12(in1[12],in2[12],c11,out[12],c12);
	fullAdder FA13(in1[13],in2[13],c12,out[13],c13);
	fullAdder FA14(in1[14],in2[14],c13,out[14],c14);
	fullAdder FA15(in1[15],in2[15],c14,out[15],c15);

  assign out[16] = c15;

endmodule
