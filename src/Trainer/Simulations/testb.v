
`timescale 1ns / 1ps

module testbench();

reg rstn;
reg clock;

initial begin
  $display($time, " << Starting the Simulation >>");
  rstn = 1'b0;
  clock = 0;
  #5 rstn = 1'b1;
end

always #1 clock=~clock;

memModule mod_memModule();

/*
initial
begin
      $dumpfile("RC_Testbench.vcd");
      $dumpvars;
end
*/
/*
initial begin
  #10000;
  $display("<< Simulation ended. >>");
  $finish;
end
*/
endmodule