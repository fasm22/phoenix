import sys
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

sys.path.insert(0, '../')

def charArray2Int(pArray):
    result = 0
    
    digits = 2
    for i in range(0,3):
        if(pArray[2-i] != ''):
            try:
                result += int(pArray[2-i]) * pow(10,2-digits)
                digits-=1
            except:
                #pass
                result = 0

    return result

try:
    with open("imgs/"+str(sys.argv[1])) as file:
        width = int(sys.argv[2])
        heigth = int(sys.argv[3])

        outImage = [[]]

        i = 0

        cWidth = 0
        cHeigth = 0
        while(i < width*heigth):

            pixelBuffer = ["","",""]
            charBuffer = ""
            isPixel = True

            for j in range(0,3):
                charBuffer = str(file.read(1))
                
                #print(charBuffer)
                #Verify if byte is "," | "\n" | " "
                #When j=0 skip to next valid number.
                if(j==0 and (charBuffer == "," or 
                             charBuffer == "\n"  or 
                             charBuffer == " ") ):
                    isPixel = False
                    break
                
                #When j != 0, continue to next pixel because
                #current pixel has less than 3 digits
                if(j!=0 and (charBuffer == "," or 
                             charBuffer == "\n"  or 
                             charBuffer == " ") ):
                    break
                
                pixelBuffer[j] = charBuffer


            if(isPixel):
                if(cWidth < width):
                    outImage[cHeigth].append(charArray2Int(pixelBuffer))
                    cWidth+=1
                else:
                    outImage.append([])
                    cHeigth+=1
                    cWidth = 1
                    outImage[cHeigth].append(charArray2Int(pixelBuffer))
                i+=1

        imageArray = np.asarray(outImage)

        #plt.imshow(imageArray, cmap='gray')
        #plt.show()
        print("INFO: Saving image.")
        saveName = "imgs/"+sys.argv[1][0]+sys.argv[1][1]+"image.jpg"
        plt.imsave(saveName,imageArray,cmap='gray')

    '''
    image = imread()
    image_w = image_l= len(image[0])
    
    file = open("build/image.txt","w") 

    for i in range(0,image_w):
        for j in range(0,image_l):
            nextPixel = str(image[i][j])

            if(j+1 != image_l):
                nextPixel+=","

            file.write(nextPixel)#Add next pixel to current row

        file.write("\n"); #Add new line for the next image row        
    
    file.close()'''

except IOError:
    print("Error: not such file or directory.")
    print("Please verify the raw image is in the imgs/ directory.")

 