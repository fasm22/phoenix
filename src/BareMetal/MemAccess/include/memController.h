#ifndef _MEM_CONTROLLER_H
#define _MEM_CONTROLLER_H

/******************MACROS TO CONTROL THE BEHAVIOUR OF PROGRAM*************/

#define SWITCH_ON_CACHE         //Uncomment to switch on cache and use ACP
#define DMA_TRANSFER_SIZE  256  //DMA transfer size in Bytes

#define DO_QUALITY_CONTROl      //Uncommnent to perfom runtime-quality control

#define DO_AUTO_EXECUTION         //Uncomment to control the program with user button (key3)

/*****************************************************************************/
#ifndef soc_cv_av
    #define soc_cv_av
#endif

#include "hwlib.h"
#include <stdbool.h>
#include "socal/hps.h"
#include "socal/socal.h"
#include "socal/alt_acpidmap.h"
#include "alt_address_space.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>



#include "arm_cache_modified.h" //To access cache_configuration required methods
#include "fpga_dmac_api.h"



//The address of FPGA-DMAC is the HPS-FPGA bridge + Qsys address assigned to it
#define FPGA_DMAC_QSYS_ADDRESS 0x10000
#define FPGA_DMAC_ADDRESS ((uint8_t*)0xC0000000+FPGA_DMAC_QSYS_ADDRESS)
//Address of the On-Chip RAM in the FPGA, as seen by processor
#define FPGA_OCR_QSYS_ADDRESS_UP 0x0
#define FPGA_OCR_ADDRESS_UP ((uint8_t*)0xC0000000+FPGA_OCR_QSYS_ADDRESS_UP)
//Address of the On-Chip RAM in the FPGA, as seen by DMAC
#define FPGA_OCR_ADDRESS_DMAC 0x0

//DMAC transfer addresses (from processor they are virtual addresses)
#ifdef  SWITCH_ON_CACHE
  //add 0x80000000 to access through ACP
  #define DMA_TRANSFER_SRC_DMAC   ((uint8_t*) &Buffer[0] + 0x80000000)
#else
  #define DMA_TRANSFER_SRC_DMAC   ((uint8_t*) Buffer)
#endif
#define DMA_TRANSFER_SRC_UP       ((uint8_t*) Buffer)
#define DMA_TRANSFER_DST_DMAC     ((uint8_t*) FPGA_OCR_ADDRESS_DMAC)
#define DMA_TRANSFER_DST_UP       ((uint8_t*) FPGA_OCR_ADDRESS_UP)

#define HPS_TO_FPGA_INTERRUPT     ((uint8_t*) FPGA_OCR_ADDRESS_UP)        //FPGA: 0x0000 0000
#define FPGA_TO_HPS_INTERRUPT     ((uint8_t*) FPGA_OCR_ADDRESS_UP+0x20)   //FPGA: 0x0000 0020
#define CYCLE_COUNTER             ((uint8_t*) FPGA_OCR_ADDRESS_UP+0x40)   //FPGA: 0x0000 0040
#define DMA_INPUT_IMAGE           ((uint8_t*) FPGA_OCR_ADDRESS_UP+0x200000)   //FPGA: 0x0020 0000
#define DMA_OUTPUT_IMAGE          ((uint8_t*) FPGA_OCR_ADDRESS_UP+0x220000)   //FPGA: 0x0020 0000
#define IMAGE_SIZE_WIDTH          128
#define IMAGE_SIZE_HEIGHT         128

//GPIO to change AXI AXI_SIGNALS
#define GPIO_QSYS_ADDRESS 0x20000
#define GPIO_ADDRESS ((uint8_t*)0xC0000000 + GPIO_QSYS_ADDRESS)

/************MACRO TO SELECT THE CACHE CONFIGURATION WHEN CACHE IS ON*********/

//With this define select the level of cache optimization
/*
0 no cache
(basic config and optimizations)
1 enable MMU
2 do 1 and initialize L2C
3 do 2 and enable SCU
4 do 3 and enable L1_I
5 do 4 and enable branch prediction
6 do 5 and enable L1_D
7 do 6 and enable L1 D side prefetch
8 do 7 and enable L2C
(special L2C-310 controller + Cortex A9 optimizations)
9 do 8 and enable L2 prefetch hint
10 do 9 and enable write full line zeros
11 do 10 and enable speculative linefills of L2 cache
12 do 11 and enable early BRESP
13 do 12 and store_buffer_limitation
*/
#define CACHE_CONFIGURATION 9


/************** METHODS DEFINITIONS ****************/
int initMemory();

void startInterruptVectors();

void transferToMemory(uint8_t* pMemData, int pWidth, int pHeight);

uint8_t* getImageFromMemory(int pWidth, int pHeight);

void executeAccelerator();

void writeInterrupt(short pVectorPosition, short pHexValue);

void print_src_dst(uint8_t* src, uint8_t* dst, int size);

void cache_configuration(int cache_config);

uint8_t getInterruptCode(uint8_t* src);

#endif