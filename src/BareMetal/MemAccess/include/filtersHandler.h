#ifndef _FILTERS_HANDLER_H
#define _FILTERS_HANDLER_H

#include "../include/filters.h"
#include "../include/memController.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

enum FILTER_TYPE {GAUSSIAN, SOBEL, LAPLACE};

uint8_t* applyFilterToImage(uint8_t pImage[], int width, int height, enum FILTER_TYPE pType);

void controlImageQuality(int pIndex, int width, int height, enum FILTER_TYPE pType, uint8_t* pRefImage, uint8_t pImage[]);

uint8_t getPixelDifference(uint8_t pExactResult, uint8_t pApproxResult);

uint8_t execFilter(int a, int b, int c, int d, int e, int f, int g, int h, int i, enum FILTER_TYPE pType);

#endif