#ifndef _IMAGEHANDLER_H
#define _IMAGEHANDLER_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../include/diskio.h"
#include "../include/ff.h"		//Declarations of FatFs API


uint8_t charArray2Int(char* pArray);

int getCharSize(uint8_t pNumber);

void saveData(uint8_t pImage[], char* pFilename, int pWidth, int pHeight);

uint8_t* loadData(const char* pFilename, int pWidth, int pHeight);

#endif