#ifndef _FILTERS_H
#define _FILTERS_H

#include <stdint.h>

void laplace8(uint8_t* out, int a, int b, int c, int d, int e, int f, int g, int h, int i);

void gaussian(uint8_t* out, int a, int b, int c, int d, int e, int f, int g, int h, int i);

void sobel(uint8_t* out, int a, int b, int c, int d, int f,int g, int h, int i);


#endif