#ifndef _INTERR_CONTROLLER_H
#define _INTERR_CONTROLLER_H

#include <stdlib.h>
#include <stdio.h>


#include "../include/memController.h"
#include "../include/imageHandler.h"
#include "../include/filtersHandler.h"

#include "../include/pmu.h" //Time counter

void showSystemInformation();
void setup();
void handleInterrupts();

#endif