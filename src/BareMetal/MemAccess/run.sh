#!/bin/bash


if [ $# -eq 1 ] || [ $# -eq 2 ] || [ $# -eq 3 ] || [ $# -eq 4 ]; then
    if [ "$1" = "-h" ]; then
        echo -n "
    ############################################################################################
    Phoenix Script.

    Options:
    -img2c            Convert image to header file. Usage: ./run.sh -img2c file.jpg
    -img2raw          Convert image to raw file. Usage: ./run.sh -img2raw file.jpg
    -raw2img          Convert ram file to image. Usage: ./run.sh -raw2img file.txt width heigth
    -build            Make the project and burn to SD card. Usage: ./run.sh -build systemNumber
                      System Numbers available:
                        1   Exact version
                        2   Approx version (without I)
                        3   Approx version (without I and skip I access)
                        4   Approx version (E=F and skip F access)
                        5   Approx version (E=F,B=C and skip C,F access)
                        6   Approx version (E=F,B=C,G=H and skip C,F,H access)
    
    -getimage         Get the OUTPUT.txt image file from the SD card partition 1.
    -h, --help        Display this help and exit
    -v,               Display version of the script.
    ############################################################################################
    "

    elif [ "$1" = "-img2c" ]; then
        if [ "$#" -eq 2 ]; then
            echo "INFO: Converting image file '$2' to header file include/imageArray.h"
            python config/image2c.py $2
            echo "INFO: Done."
        else
            echo "ERROR: Please add the image file as the second parameter."
        fi
    elif [ "$1" = "-img2raw" ]; then
        if [ "$#" -eq 2 ]; then
            echo "INFO: Converting image file '$2' to raw file build/image.txt"
            python config/image2raw.py $2
            echo "INFO: Done."
        else
            echo "ERROR: Please add the image file as the second parameter."
        fi
    elif [ "$1" = "-raw2img" ]; then
        if [ "$#" -eq 4 ]; then
            echo "INFO: Converting raw file '$2' to image file build/outImage.jpg"
            python config/raw2image.py $2 $3 $4
            echo "INFO: Done."
        elif [ "$#" -eq 2 ]; then
            echo "ERROR: Please the width as 3rd parameter and the height as 4th parameter."
        elif [ "$#" -eq 3 ]; then
            echo "ERROR: Please the width as 3rd parameter and the height as 4th parameter."
        else
            echo "ERROR: Please add the image file as the second parameter."
        fi
    elif [ "$1" = "-getimage" ]; then
        if [ "$#" -eq 3 ]; then
            echo "INFO: Mounting SD Card (/dev/mmcblk0p1)"
            mkdir -p sdcard/
            sudo mount /dev/mmcblk0p1 sdcard/
            echo "INFO: Copying files from SD Card."
            sudo cp sdcard/SWIMAGE.TXT imgs/
            sudo cp sdcard/HWIMAGE.TXT imgs/
            echo "INFO: Converting images."
            python config/raw2image.py "SWIMAGE.TXT" $2 $3
            python config/raw2image.py "HWIMAGE.TXT" $2 $3
            sudo umount sdcard/
            sudo rm -rf sdcard/
            echo "INFO: Done."
        else
            echo "ERROR: Please the width as 2nd parameter and the height as 3rd parameter."
        fi
    elif [ "$1" = "-build" ]; then
        if [ "$#" -eq 2 ]; then
            /bin/bash runAux.sh $2
            echo "INFO: Making program."
            make
            echo "INFO: Cleaning files."
            make clean

            echo "INFO: Mounting SD Card (/dev/mmcblk0p1)"
            mkdir -p sdcard/
            sudo mount /dev/mmcblk0p1 sdcard/
            echo "INFO: Copying files to SD Card."
            sudo cp build/soc_system.rbf sdcard/
            sudo cp build/baremetalapp.bin.img sdcard/
            sudo cp build/baremetalapp.bin sdcard/
            sudo cp build/u-boot.img sdcard/
            sudo cp build/u-boot.scr sdcard/
            sudo cp build/image.txt sdcard/
            sudo umount sdcard/
            sudo rm -rf sdcard/
            echo "INFO: Done."
        else
            echo "ERROR: Please the insert the system code as the 2nd parameter. Run ./run.sh -h for more information"
        fi
    fi
else
    echo "No arguments provided. Please use -h to further information."
fi
