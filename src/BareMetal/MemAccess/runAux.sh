#!/bin/bash

modesArray=("None" "Exact version" "Approx version (without I)" "Approx version (without I and skip I access)" "Approx version (E=F and skip F access)" "Approx version (E=F,B=C and skip C,F access)" "Approx version (E=F,B=C,G=H and skip C,F,H access)")

if [ "$1" -gt 0 ] && [ "$1" -lt 7 ]; then
    cp build/systems/sys_$1.rbf build/
    rm build/soc_system.rbf
    echo "INFO: Loading system $1 ${modesArray[$1]}..."
    mv build/sys_$1.rbf build/soc_system.rbf
elif [ "$1" -eq 8 ]; then
    echo "INFO: Loading quartus generated file..."
else
    echo "ERROR: Unknown parameter for runAux.sh file. Please try again."
fi