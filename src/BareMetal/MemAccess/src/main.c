/******************************************************************************
* Chair for Embedded Systems
* Karlsruhe Institute of Technology
* Tecnológico de Costa Rica
*
* Fabián Solano M.
*
******************************************************************************/

#include "../include/interruptController.h"
#include <stdio.h>


int main(void){
    printf("Starting system...\n\r");

    showSystemInformation();
    
    setup();

    handleInterrupts();
    
    return 0;
}
