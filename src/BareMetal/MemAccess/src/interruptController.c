#include "../include/interruptController.h"


void showSystemInformation(){
    printf("\n\r");
    printf("##################################\n\r");
    printf("      Starting Phoenix System     \n\r");
    printf("##################################\n\r");
    printf("       Fabian Solano Madriz       \n\r");
    printf("         CES | KIT | TEC          \n\r");
    printf("               v1.1               \n\r");
}

void setup(){
    initMemory();
    startInterruptVectors();

    pmu_init_ns(800, 1); //Initialize PMU cycle counter, 800MHz source, frequency divider 1
	pmu_counter_enable();//Enable cycle counter inside PMU (it starts counting)
	float pmu_res = pmu_getres_ns();
	printf("INFO: PMU cycle counter resolution is %f ns\n\r", pmu_res );	
}

void handleInterrupts(){
    int value = 0xFA;

    int width = IMAGE_SIZE_WIDTH;
    int height = IMAGE_SIZE_HEIGHT;
    int maxSize = width*height;
    enum FILTER_TYPE cpType = GAUSSIAN;
    
    uint8_t* image = loadData("image.txt",width,height);

    uint8_t* swImageResult = loadData("image.txt",width,height);
    
    #ifdef DO_AUTO_EXECUTION //If auto_execution is defined in memController.h
    while(1){
        int currentValue = (int)getInterruptCode(HPS_TO_FPGA_INTERRUPT);

        if(value != currentValue){
            printf("Command $>: %04X\n\r",currentValue);
            value = currentValue;
        }

        if(value == 0x25){
            writeInterrupt(0,0x26);
            transferToMemory(image,width,height);
            writeInterrupt(0,0x27);
        }
        
        else if(value == 0x27){
            executeAccelerator();
            writeInterrupt(0,0x29);
        }
        
        else if(value == 0x29){
            writeInterrupt(0,0x2A);
            
            unsigned int pmu_counter_ns; //Variable to read counter
            pmu_counter_reset(); //Reset time counter
            
            swImageResult = applyFilterToImage(image,width,height,cpType);

            pmu_counter_read(&pmu_counter_ns); //Read counter
            //printf("INFO: Software filter applied. Sofware cycles: %d \n\r",pmu_counter_ns);

            saveData(swImageResult,"swimage.txt",width,height);

            writeInterrupt(0,0x2B);
        }

        
        else if(value == 0x2B){
            
            unsigned int pmu_counter_ns; //Variable to read counter
            pmu_counter_reset(); //Reset time counter
            
            int interruptCode = 0; //Read code to handle the interruption from FPGA
            int index = width + 1; //Start index to control error method
            
            //Wait for the hardware to apply the corresponding operations
            while(1){
                interruptCode = getInterruptCode(FPGA_TO_HPS_INTERRUPT);
                if(interruptCode == 0xEF){
                    pmu_counter_read(&pmu_counter_ns); //Read counter
                    printf("INFO: Hardware filter applied. Sofware cycles: %d \n\r",pmu_counter_ns);
                    break;
                }
                #ifdef DO_QUALITY_CONTROl //If QC is defined at memController.h
                else{
                    controlImageQuality(index,width,height,cpType,swImageResult,image);
                    index += 15;

                    if(index > maxSize)
                        index = width + 1;
                }
                #endif
            }
            
            printf("INFO: Cycles used: %d\n\r",(uint8_t)CYCLE_COUNTER[0]);

            writeInterrupt(0,0x2C);
            
            image = getImageFromMemory(width, height);

            saveData(image,"hwimage.txt",width,height);
            break;
        }
        
    }

    #else//Execute the program manually by using the user button (key3)
    while(1){
        int currentValue = (int)getInterruptCode(HPS_TO_FPGA_INTERRUPT);

        if(value != currentValue){
            printf("Command $>: %04X\n\r",currentValue);
            value = currentValue;
        }

        if(value == 0x25){
            writeInterrupt(0,0x26);
            transferToMemory(image,width,height);
        }
        
        else if(value == 0x27)
            executeAccelerator();
        
        else if(value == 0x29){
            writeInterrupt(0,0x2A);
            
            unsigned int pmu_counter_ns; //Variable to read counter
            pmu_counter_reset(); //Reset time counter
            
            swImageResult = applyFilterToImage(image,width,height,cpType);

            pmu_counter_read(&pmu_counter_ns); //Read counter
            printf("INFO: Software filter applied. Sofware cycles: %d \n\r",pmu_counter_ns);

            saveData(swImageResult,"swimage.txt",width,height);

            writeInterrupt(0,0x2B);
        }

        
        else if(value == 0x2B){
            
            unsigned int pmu_counter_ns; //Variable to read counter
            pmu_counter_reset(); //Reset time counter
            
            int interruptCode = 0; //Read code to handle the interruption from FPGA
            int index = width + 1; //Start index to control error method
            
            //Wait for the hardware to apply the corresponding operations
            while(1){
                interruptCode = getInterruptCode(FPGA_TO_HPS_INTERRUPT);
                if(interruptCode == 0xEF){
                    pmu_counter_read(&pmu_counter_ns); //Read counter
                    printf("INFO: Hardware filter applied. Sofware cycles: %d \n\r",pmu_counter_ns);
                    break;
                }
                #ifdef DO_QUALITY_CONTROl //If QC is defined at memController.h
                else{
                    controlImageQuality(index,width,height,cpType,swImageResult);
                    index += 15;

                    if(index > maxSize)
                        index = width + 1;
                }
                #endif
            }
            
            printf("INFO: Hardware cycles used: %d\n\r",(uint8_t)CYCLE_COUNTER[0]);

            writeInterrupt(0,0x2C);
            
            image = getImageFromMemory(width, height);

            saveData(image,"hwimage.txt",width,height);

            break;
        }
    }
    #endif

    printf("INFO: Program ended succesfully.");
    
}