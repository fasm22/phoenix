#include "../include/filtersHandler.h"

uint8_t* applyFilterToImage(uint8_t pImage[], int width, int height, enum FILTER_TYPE pType){
    
    int index = width + 1 ; //Start on width index to skip first row
                            //+1 to skip first column

    uint8_t* resImage = calloc(width*height,sizeof(uint8_t));     
    
    for(int i = 0; i < height - 2; i++){
        for(int j = 0; j < width - 1; j++){ //Skip first column
            uint8_t result;
            
            if(j < width - 2){ //Skip last column
                result = execFilter(
                        pImage[index - width - 1], pImage[index - width], pImage[index - width + 1], //a,b,c
                        pImage[index - 1], pImage[index], pImage[index + 1],                         //d,e,f
                        pImage[index + width - 1], pImage[index + width], pImage[index + width + 1], //g,h,i
                        pType);

                resImage[index] = result;
            }
            index++;
        }
        index++;
    }

    return resImage;
}

void controlImageQuality(int index, int width, int height, enum FILTER_TYPE pType, uint8_t* pRefImage, uint8_t pImage[]){

    uint8_t hardwareResult = DMA_OUTPUT_IMAGE[index+1];
    //uint8_t softwareResult = pRefImage[index];

    uint8_t softwareResult = execFilter(
                        pImage[index - width - 1], pImage[index - width], pImage[index - width + 1], //a,b,c
                        pImage[index - 1], pImage[index], pImage[index + 1],                         //d,e,f
                        pImage[index + width - 1], pImage[index + width], pImage[index + width + 1], //g,h,i
                        pType);

    if(getPixelDifference(softwareResult,hardwareResult) > 0)
        DMA_OUTPUT_IMAGE[index] = softwareResult;
}

uint8_t getPixelDifference(uint8_t pExactResult, uint8_t pApproxResult){
    return abs(pExactResult - pApproxResult);
}

uint8_t execFilter(int a, int b, int c, int d, int e, int f, int g, int h, int i, enum FILTER_TYPE pType){
    uint8_t result;

    if(pType == GAUSSIAN)
        gaussian(&result,a,b,c,d,e,f,g,h,i);
    
    else if(pType == LAPLACE)
        laplace8(&result,a,b,c,d,e,f,g,h,i);
    
    else if(pType == SOBEL)
        sobel(&result,a,b,c,d,f,g,h,i);

    return result;
}