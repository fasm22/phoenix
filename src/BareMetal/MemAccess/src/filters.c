#include "../include/filters.h"

void laplace8(uint8_t* out, int a, int b, int c, int d, int e, int f, int g, int h, int i) {

	int s1 = a + b; //8 -> 9
	int s2 = c + d; //8 -> 9
	int s3 = f + g; //8 -> 9
	int s4 = h + i; //8 -> 9

	int s5 = s1 + s2; //9 -> 10
	int s6 = s3 + s4; //9 -> 10

	int s7 = s5 + s6; //10 -> 11

	int e8 = e * 8; //8 -> 11

	int res = e8 - s7; //11 -> 11
  
  *out = (uint8_t)res;
}

void gaussian(uint8_t* out, int a, int b, int c, int d, int e, int f, int g, int h, int i) {

	int b2 = b * 2; //8 -> 9
	int d2 = d * 2; //8 -> 9
  int e4 = e * 4; //8 -> 10
	int f2 = f * 2; //8 -> 9
	int h2 = h * 2; //8 -> 9

	int s1 = a + b2; //9 -> 10
	int s2 = c + d2; //9 -> 10
	int s3 = e4 + f2; //10 -> 11
	int s4 = g + h2; //9 -> 10

	int s5 = s1 + s2; //10 -> 11
	int s6 = s3 + s4; //11 -> 12

	int s7 = s5 + s6; // 12 -> 13

	int s8 = s7 + i; //13 -> 14

	int res = s8 >> 4;
  
  *out = (uint8_t)res;
}

void sobel(uint8_t* out, int a, int b, int c, int d, int f, int g, int h, int i) {

  // Gx
	int x1 = a + g; //8 -> 9
	int x2 = d * 2; //8 -> 9
	int x3 = x1 + x2; //9 -> 10

	int x4 = c + i; //8 -> 9
	int x5 = f * 2; //8 -> 9
	int x6 = x4 + x5; //9 -> 10

	int gx = x6 - x3;

  // Gy
	int y1 = a + c; //8 -> 9
	int y2 = b * 2; //8 -> 9
	int y3 = y1 + y2; //9 -> 10

	int y4 = g + i; //8 -> 9
	int y5 = h * 2; //8 -> 9
	int y6 = y4 + y5; //9 -> 10

	int gy = y3 - y6;

	int res = gx + gy;

  *out = (uint8_t)res;
}

/*
int main() {
  int res;
  laplace4(&res,23,43,54,23,25);
  return 0;
}*/
