#include "../include/imageHandler.h"


uint8_t charArray2Int(char* pArray){
    uint8_t output = 0;

    short digits = 2;
    for(int i = 2; i > -1; i--){
        uint8_t temp = ((uint8_t)pArray[i]);
        if(temp >=48 && temp <= 57){
            temp-=48;
            output += temp * pow(10,2-digits);
            digits--;
        }
    }
    return output;
}

int getCharSize(uint8_t pNumber){
    int res = 1;

    if(pNumber >= 100)
        res = 3;
    else if(pNumber >= 10)
        res = 2;
    else
        res = 1;
    
    return res;
}

void saveData(uint8_t pImage[], char* pFilename, int pWidth, int pHeight){
    FATFS FatFs;		//FatFs work area needed for each volume
    FIL Fil;			//File object needed for each open file
    UINT bw;            //Byte reader counter
    int index = 0;
    
    f_mount(&FatFs, "", 0);		//Give a work area to the default drive

    if (f_open(&Fil, pFilename, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK) {
    //if (f_open(&Fil, "output.txt", FA_WRITE | FA_CREATE_ALWAYS) == FR_OK) {
        for(int i = 0; i < pHeight ; i++){
            for(int j = 0; j < pWidth; j++){ //To heigth-2 to skip last row

                short charSize = getCharSize(pImage[index]);
                char str[charSize];
                sprintf(str, "%d", pImage[index]);

                f_write(&Fil,str,charSize, &bw);

                if(j+1 == pHeight)
                    f_write(&Fil,"\n",1, &bw);
                else
                    f_write(&Fil, ",", 1, &bw);

                index++;
            }
            //index++;
        }
        f_close(&Fil);
        printf("INFO: Output image wrote to SD Card as '%s'\n\r",pFilename);
    }
    else
        printf("ERROR: Failed to write image to SD Card...");
}

uint8_t* loadData(const char* pFilename, int pWidth, int pHeight){
    FATFS FatFs;		//FatFs work area needed for each volume
    FIL Fil;			//File object needed for each open file
    UINT bw;            //Byte reader counter
    int pMaxSize = pWidth * pHeight;
    uint8_t* outImage = calloc(pMaxSize,sizeof(uint8_t)); //Output data of the image
    
    printf("Mouting filesystem...\n\r");

	f_mount(&FatFs, "", 0);		//Give a work area to the default drive

    if (f_open(&Fil, pFilename, FA_READ) == FR_OK){
        int i = 0;
        
        
        while(i < pMaxSize){
            char* pixelBuffer = calloc(3,sizeof(char));
            char* charBuffer  = calloc(1,sizeof(char));
            
            short isImage = 1;

            for(short j=0; j < 3; j++){
                f_read(&Fil, charBuffer, 1, &bw);

                //Verify if ASCII Code for "," = 44 | "\n" = 10 | " " = 32
                //When j=0 skip to next valid number.
                if(j == 0 && (charBuffer[0] == 44 || 
                             charBuffer[0] == 32 || 
                             charBuffer[0] == 10)) {
                    isImage = 0;
                    break;
                }
                //When j != 0, continue to next pixel because
                //current pixel has less than 3 digits
                else if(j != 0 && (charBuffer[0] == 44 || 
                                   charBuffer[0] == 32 || 
                                   charBuffer[0] == 10)) {
                    break;
                }
                
                pixelBuffer[j] = charBuffer[0];
            }

            if(isImage){
                outImage[i] = charArray2Int(pixelBuffer);
                i++;
            }
        }
        f_close(&Fil);
        return outImage;
    }
    else{
        printf("Failed to open file '%s'...\n\r",pFilename);
        return outImage;
    }
}