import sys
import os
from matplotlib.image import imread


sys.path.insert(0, '../')


try:
    image = imread("imgs/"+str(sys.argv[1]))
    image_w = image_l= len(image[0])
    
    file = open("build/image.txt","w") 

    for i in range(0,image_w):
        for j in range(0,image_l):
            nextPixel = str(image[i][j])

            if(j+1 != image_l):
                nextPixel+=","

            file.write(nextPixel)#Add next pixel to current row

        file.write("\n"); #Add new line for the next image row        
    
    file.close()

except IOError:
    print("Error: not such file or directory.")
    print("Please verify the image is in the imgs/ directory.")

 