import sys
import os
from matplotlib.image import imread


sys.path.insert(0, '../')


try:
    image = imread("imgs/"+str(sys.argv[1]))
    image_w = image_l= len(image[0])
    
    file = open("include/imageArray.h","w") 
    file.write("#ifndef _IMAGEARRAY_H\n")
    file.write("#define _IMAGEARRAY_H\n\n")
    
    file.write("#include <stdlib.h>\n")
    file.write("#include <stdint.h>\n\n")
    
    file.write("uint8_t* memAlloc;\n\n")
    
    file.write("void cleanArray(){\n")
    file.write("\tfree(memAlloc);\n")
    file.write("};\n\n")

    file.write("void initImageArray(){\n")
    nLine = "\tmemAlloc = calloc ("+str(image_l*image_w)+",1);\n"
    file.write(nLine)

    memPosCount=0
    for i in range(0,image_w):
        for j in range(0,image_l):
            nextLine = "\tmemAlloc[" + str(memPosCount) + "] = " 
            nextLine += str(image[i][j]) +";"
            nextLine += "\t\t\t//" + str(hex(image[i][j])) + "\n"
            file.write(nextLine)
            memPosCount+=1

    file.write("};\n\n")

    file.write("#endif")
        
    file.close()

except IOError:
    print("Error: not such file or directory.")
    print("Please verify the image is in the imgs/ directory.")

 