\chapter{Background and Related Work}
\label{ch:marco}

\section{Approximate Computing}
Both academia and industry have developed lots of advances on semiconductors 
technologies and energy efficient systems, but the total energy consumption quota of electronic devices is still growing at an alarming rate on response to the need of data processing that is growing at even a bigger rate. These gadgets steadily increase its interaction level with the physical world. For example, by using sensors to obtain relevant information from their surroundings, and then process an enourmous quantity of data retrieved from arrays of these inputs from the world. Multiple applications from areas such as pattern recognition, data mining and information synthesis (RMS) have appeared, and represent a significant portion of the computational resources available \cite{AproxSurvey}. 

Approximate computing is a design paradigm that uses several techniques to indulge the processing needs from applications in multiple areas such as multimedia, machine learning and computer vision. If these techniques are applied, the results from their operations are not  100\% numerically correct. However, due to the nature of the tasks that are being
processed the results are still relevant and useful to the final user, with some
advantages such as circuit area reduction, less power consumption or better
performance. This opens a new research field to find the balance point between
performance and energy consumption without affecting the output of the calculations.
Nevertheless, a great challenge is quality control, and how to meet the user
quality requirements. To understand the problem and explore solutions is necessary
to review some relevant theorical concepts and definitions.

\subsection{Definitions}
The key concept that must be understood is \textbf{Approximate Computing}, which
refers to a design paradigm, that through the use of different methods aims to 
relax the numerical operations in an architecture. By deliberately introducing 
"acceptable errors" into computing processes it guarantees significant energy - 
efficiency gains \cite{AproxSurvey}. On figure \ref{fig:overallFramework} the
most relevant basic modules for a given error-tolerant application that can be 
exploited by approximate computing are shown. Some of the key components are
explained below, however all of them have been widely discussed \cite{AproxSurvey}.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[width=400pt]{overallFramework}
      \caption[Overall Framework of AC]{Overall Framework of Approximate Computing \cite{AproxSurvey}.}
      \label{fig:overallFramework}
  \end{center}
\end{figure}

An \textbf{Approximate Kernel} is the specific technique used to do 
approximations. There are two main scopes, hardware layer and software layer. The
first one might use less accurate but more energy efficient circuits for calculations or reduce supply voltage on purpose to tradeoff energy and accuracy. Also others approaches include ISA extensions, analog computing and approximate storage. On the other hand, at software layer, methods could include loop perforation, thread fusion, pattern reduction, among others.

In the same way that it would be too naive to try to do optimizations with
parallel programming using an interface like \textit{OpenMP}, to a program that is 
linear, in applications that are error-tolerant, there exist error sensitive
parts of the source code, and using approximations there might lead to fatal errors.
The \textbf{Resilence Identification}, centralizes its efforts on recognizing these
sensitive blocks, some as control flow can be critical, but other can be resilent 
like the iterative methods.

The \textbf{Dynamic Quality Management} focuses on the evaluation of the intermediate
computation quality, often with a application specific light-weight checker. This
online verification system determines when to use approximate kernels and their
computation modes at execution time.

If to handle error, approximate accelerators check the output once in every \textit{n}
invocations of the approximate kernel, they use the \textbf{Periodic Control}
mecanism. Its main charactheristic is the comparison of the quality between 
approximate computation and its exact counterpart to adjust the approximate modes
for the subsequent computations. Looking after the error by using a light tool
that always check for the current status of the error is called a \textbf{Continous
Control} mecanism. Even though this method is way more intrusive it guarantees that
the error will always remain below the max value allowed.\cite{AproxKozen}

When a programming language allows the programmer to express how and where randomness
can impact the results of their source code, it is called an \textbf{Approximation
Aware Programming Language}. In \cite{AproxKozen}, this was highligthed in 1979,
showing the need for a syntax and semantics to specify the use of randomness during
runtime. Also if a compiler can use approximations inferred or explicity expressed,
in addition a previous analysis, it is called an \textbf{Approximation Aware Compiler}.
This kind of tool will transform and in result, change the semantics of the programs
to exchange the accuracy of the result for the improved performance and or energy
consumption. \textbf{Loop perforation} is a software method that modifies the
program to perform just a fraction of the total loop iterations and therefore achieve
a better performance, by running in less time. \cite{AproxSurvey}

\textbf{Approximate Processor Architectures} support the execution of approximate
software, and can be grouped in two categories. The first group aims to allow the
computation by approximate methods for traditional code running on general-purpose
processors, but with the ability to execute certain code segments in approximation
mode. On the other side, the second group converts approximable segments of 
normal code into a neurally inspired algorithm running on accelerators
\cite{AproxSurvey}.

The smallest block necessary to perform inexact computations is called \textbf{Approximate Arithmetic Unit}. Since the adder is one of the most common arithmetic circuits many authors have proposed several versions. In \cite{AproxAdder}, a transistor level approximation is used to build approximate adder cells. If compared to a classic adder cell, the layout area of the approximate version is just one-third of it.

\section{Periodic Quality Control}
Usually, an approximate accelerator replaces each invocation of a commonly executed region of source code without taking in consideration the final quality degradation. However, there is a huge decision space where each invocation can be delegated to the accelerator, improving performance and efficiency. On the other hand, if it is run on the precise core the quality is maintained. There are four main challenges that must be considered \cite{PeriodicStatistical}.

\begin{enumerate}
  \item What accelerator charactheristic can guide the design of a framework? Over a series of different tests it is shown that only a small fraction of application output (0-20\%) have large errors (difference of 25\% to 65\%).
  \item What information is needed and is available to feed accelerator input that cause large error? The accelerator error is a function of the input vectors and the accelerator configuration. For a given application, the accelerator configuration is fixed, making the accelerator error just a function of the input vector.
  \item How to map the final output quality loss as a local accelerator error? By making local decisions based on the accelerator input without a general view of execution and without knowing about the manifestation of the final accelerator error output.
  \item What guarantees are provided for the final output quality loss? The final output quality can be mapped onto the accelerator as the threshold for the accelerator error, and could be provided by formal or statistical guarantees.
\end{enumerate}

MITHRA's framework, proposed in \cite{PeriodicStatistical}, shown in figure \ref{fig:statisticalDiagram} comprises a compiler constituting statistical optimizer and pre-trainer, and a hardware classifier which is a microarchitectural mechanism that resides on the accelerator and the main core. By using the accelerator inputs the hardware classifier makes a binary decision of running the original function on the core or invoke the accelerator. It is necessary to train the hardware classifier in order to make decisions at runtime.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[width=450pt]{StadisticalDiagram}
      \caption[Overview of MITHRA system]{Overview of MITHRA comprising a statistical optimizer, a trainer, and a hardware classifier \cite{PeriodicStatistical}. }
      \label{fig:statisticalDiagram}
  \end{center}
\end{figure}

Finding the threshold step, is required to tune the quality control knob. The threshold enables MITHRA to maximize the accelerator invocations for any level of quality loss. To allow an accelerator invocation, the error of all the elements must be below the threshold, which is given by:

\begin{equation} \label{equ:threshold}
  \forall{o_{i}} \in OutputVector \mid{o_{i}(precise)-o_{i}(approximate)}\space\mid \space {\leq th}
\end{equation}

This framework (Figure \ref{fig:statisticalDiagram}) provides statistical guarantees that quality requirements will be achieved on unknown datasets with high confidence. To ensure those guarantees, the algorithm counts the application input datasets that have final output quality ($q_{i}$) below or equal to the desired quality loss ($q$) as shown in:

\begin{equation} \label{equ:desiredQuality}
  \forall{i}\in{inputSet} \;\;\; {if}(q_{i}(P_{i}(th))\leq{q}) \;\;\; {n_{success}=n_{success}+1}
\end{equation}

Through the Clopper-Pearson exact method computes the one-sided confidence interval of success rate $S^{(q)}$, when the number of application inputs, $n_{trials}$, and the number of successes among the trials, $n_{success}$, are measured for a sample of the population, which is given by:

\begin{equation} \label{equ:FDistribution}
  \mbox{\Large\( %
  \frac{1}{1+\frac{n_{trials}-n_{success}+1}{n_{success}\cdot{F[1-\beta;2\cdot{n_{success},2\cdot{n_{trials}-n_{sucess}+1}}]}}}
  <
  S^{(q)}
  \)} %
\end{equation} 

where $F$ is the critical value that is calculated based on the F-Distribution. \cite{BookStatisticalMethods}

Another work scope on periodic control is proposed in \cite{PeriodicMachineLearning} as a Machine Learning based model called SMURF: Smart Model for Universal Recuperation of Hard Faults in Approximate Computing Applications. Approximate accelerators usually exploit the inherent parallelism of tasks and are often tolerant to inaccuracies in their outputs. 

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[width=220pt]{smurfModule}
      \caption[SMURF Module]{SMURF Module for Hard Fault Recuperation.
      \cite{PeriodicMachineLearning}}
      \label{fig:smurfModule}
  \end{center}
\end{figure}

SMURF compensation model experimental results showed improvement of the accuracy by 50\% and decreases the overall mean error rate by 90\% with an area overhead of 5\% compared to execution without fault compensation. This work proposes a scalable approach for building error compensation modules that focuses on the difference in the results of golden and faulty output for hardware accelerators that are error-tolerant. Also, some experiments at gate-level for different accelerators of multiple complexities, with evaluation by supervised learning predictive methods helped in the building of the compensation logic.

Most prior works focusing on error compensation are highly application dependent. However, SMURF method is composed of a tunable module making it flexible enough to compensate for different permanent faults. Figure \ref{fig:smurfModule} shows an overview of the SMURF module, which is composed by two main parts: a programmable input tree and an output adder. 

The programmable tree is composed of two main parts: a coefficient block and a comparator tree. The co-efficient block contains unique vectors that stores in wrong outputs, these are called Error Distance values ($ED$). The comparator tree is fixed and must be tuned by choosing a set of coefficients to compensate differents faults according to each accelerator of each unique chip. Finally, the n-bit adder, where $n$ is the number of outputs of the accelerator, compensates the faulty outputs by adding a compensation vector ($ED_{SMURF}$) to the faulty output value as shown in:

\begin{equation} \label{equ:OutputSMURF}
  Output_{SMURF} = Output_{Faulty} + ED_{SMURF}
\end{equation} 

The compensation tuning is divided into two main steps. The first one finds the test vectors that generate observable erroneous outputs, and the second step creates the compensation vectors to add to the faulty outputs.

\begin{enumerate}
  \item \textbf{Error Distance (ED) Vectors:} \\
        The first step is to find which input test-vectors (TVs) lead to observable erorrs in the outputs. All of the ICs are analyzed using the input patterns and their outputs compared to fault-free, golden, outputs. The ED is given by:
        \begin{equation} \label{equ:OutputSMURF}
          ED = \;\mid{Output_{Golden}-Output_{Faulty}}\space\mid
        \end{equation} 
        This step stores all the TVs and outputs. A database is generated and stored in the comparator tree of the SMURF module to identify when $n$ given input will create a wrong output.
  \item \textbf{Compensation Coefficient Supervised Learning:} \\
        Next phase analyzes the input vectors and faulty outputs pairs in order to tune the proposed compensation logic by using a supervised machine learning method. This method allows a fast, flexible, and scalable way to generate accurate compensation routines that are specific to the particular set of application inputs and permanent faults. A map between a set of input attributes and an output variable is used to predict the unknown data, to create a model of the distribution of labels according to predictor features.
\end{enumerate}

\section{Continous Quality Control}
Algorithm-specific approximation increases the programming effort because the programmer needs to write and consider about the exact and approximate versions of the software. Recently, to reduce the impact of this problem, some software and hardware techniques have been proposed such as loop perforation, approximate memoization, and analog circuits, low-power arithmetic logic units and hardware-based fuzzy memoization. However, these methods present four limitations \cite{RumbaA}: 

\begin{itemize}
  \item The output quality is dependent on the input values. It is highly possible to miss large output errors because only a small portion of them are actually examined.
  \item If the output quality drops below the acceptable threshold, there is no way to improve the quality than recompute all the outputs on exact hardware.
  \item Few output elements have considerably large errors, but these errors are able to degrade the quality.
  \item Tuning output quality based on a user's preferences is another challenge for the hardware-based approximation techniques.
\end{itemize}

The ability of applications to generate results of acceptable output quality in an approximate computing environment is mandatory to ensure a positive user experience but quality control faces challenges such as: fixing output elements with large errors is critical for user experience, output quality is input dependent, monitoring and recovering output quality is expensive as different users and applications have different requirements on output quality \cite{RumbaB}.

Three lightweight error prediction methods have been proposed \cite{RumbaA}. All of them have the ability to manage performance and accuracy tradeoffs for each application using dynamic tuning parameter. These methods leverage the idea of reexecution to fix elements with large error and obtaining a reduction in output error respect to an unchecked approximate accelerator with the same performance gain.

A block diagram of the Rumba system is presented on Figure \ref{fig:rumbaModel}. The offline block is build by two trainers. The first trainer finds the best configuration of the approximate accelerator for a particular program from its source code. The second trainer is in charge of training a simple error prediction technique based on errors generated by the accelerator trainer. The input parameters for both the approximate accelerator and the error predictor are included in the binary.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[width=450pt]{rumbaDiagram}
      \caption[Rumba Block Diagram]{High-level block diagram of the Rumba system.
      \cite{RumbaB}}
      \label{fig:rumbaModel}
  \end{center}
\end{figure}

The execution subsystem (online) is composed by the core and the accelerator. The core establishes a communication system by using I/O queues for data transfers from the core to the accelerator and viceversa. Rumba's execution process has two main components: detection and recovery modules. The design of the approximate accelerator is based on previous works \cite{NeuralAcceleration1}, but the same design principles can be used on other accelerator based computing systems to ensure the accuracy control \cite{RumbaA}.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[height=220pt]{cumulativeDistributionRumba}
      \caption[Rumba Error Distribution]{Typical cumulative distribution function of errors generated by approximation techniques. \cite{RumbaB}}
      \label{fig:rumbaDistribution}
  \end{center}
\end{figure}

Figure \ref{fig:rumbaDistribution} shows the typical CDF (Cumulative Distribution Function) of errors in the output elements of a program under approximation techniques, as reported by previous studies \cite{SamadiParaProx,NeuralAcceleration1}. This figure exemplifies how the most of the output elements (80\%) have small errors (less than 10\%). However, there are some output elements (20\%) that have significant errors. Even though, the number of elements with big errors is small, they can have huge impact on the user perception of quality.

So far it is clear that quality control is an essential and nontrivial problem in the approximate computing, some works proposed to sample a bunch of data and execute them in the approximate accelerator and the exact core \cite{AproxSage,AroxGreen}. If great differences are generated between the two computation units then a more accurate accelerator is used. However, if the accelerator consumes data that has large relative errors, the data might escape the sampling resulting in a significant degradation of the quality \cite{IterativeTraining}. 

For this reason, simply minimizing the MRE of the accelerator is not enough. To enhance the prediction accuracy, some other methods are used, such as a cache look-up table and a neural network \cite{PeriodicStatistical}. Another sophisticated tool, combines methods for several basic predictors to embrace the light-weightiness and high prediction accuracy \cite{EffectiveQualityWang}. The one-step training process, commonly used, only centers its efforts on the optimization of the approximate accelerator and the classifier separately, but a better solution exists if the both blocks are optimized as a whole.

An iterative training has been discussed as an alternative to search for the optimization of both blocks simultaneously \cite{IterativeTraining}. As shown in Figure \ref{fig:iterativeTraining}, the accelerator and the classifier are trained iteratively, and the ``garbage" training data is filtered by the classifier is prevented to be used in future iterations.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[height=150pt]{iterativeTraining}
      \caption[Iterative Training Method]{The process of iterative training \cite{IterativeTraining}}
      \label{fig:iterativeTraining}
  \end{center}
\end{figure}

On the first iteration the classifier and the accelerator are trained beforehand. Next, all the following steps are repeated. The original data is applied to the accelerator and the classifier. The output data is compared to the error-bound, and then labeled according to Table \ref{table:iterativeLabels}. Next, the training data is judiciously selected from these categories to train the accelerator and classifier in the following iteration.


\begin{table}[!ht]
    \begin{center}
    \begin{tabular}{ll}
      \multicolumn{1}{l|}{Label}& Classification                    \\ \hline
    \multicolumn{1}{l|}{A}  & Data satisfies the error bound        \\
    \multicolumn{1}{l|}{nA} & Data does not satisfy the error bound \\
    \multicolumn{1}{l|}{C}  & Data is safe-to-approximation         \\
    \multicolumn{1}{l|}{nC} & Data is not safe-to-approximation    
    \end{tabular}
    \vspace{8pt}
    \caption{Iterative Training Classification Labels}
    \label{table:iterativeLabels}
  \end{center}
\end{table}

The careful selection of the training data is the most a key factor for the iterative training method. The conceptual figure \ref{fig:iterativeGraph} illustrate how the data is partitioned. The error distribution of the data processed by the accelerator is shown in a solid curve, which is divided by the error-bond in two sections, 'A' and 'nA'. 

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[height=150pt]{iterativeGraph}
      \caption[Iterative Training Distribution]{Distribution of the four categories of the original data derived from the accelerator and the classifier \cite{IterativeTraining}.
      }
      \label{fig:iterativeGraph}
  \end{center}
\end{figure}

An ideal classifier, should identify all the 'A' category data and label them as safe-to-approximate ('C' label). The dashed line curve shows the distribution of data derived from the classifier. The four categories of the original data are classified as 'AC', 'nAC', 'AnC' and 'nAnC'. From the data obtained, the precision is given by:

\begin{equation} \label{equ:iterativePrecision}
  \frac{AC}{AC+nAC}
\end{equation} 

And the recall is obtained by:

\begin{equation} \label{equ:iterativeRecall}
  \frac{AC}{AC+AnC}
\end{equation}

In contrast to the checkpointing and rollback recovery mechanism that is commonly used in the fault-tolerant computing domain, in approximate computing it is not usual to tell whether a quality violation occurs or not, because it would violate the very purpose of the approximation. A good quality checker should be accurate and lightweight in nature. A common quality predictor is the linear model-based but it is applicable for those applications where the \textit{decision boundary} for rollback or not is linear. The combination of several basic quality predictors allows to achieve the lightweightiness while also obtaining the high prediction accuracy \cite{EffectiveQualityWang}.

As seen on Figure \ref{fig:efficientFramework} the framework targets the host-accelerator system, which is commonly used in high-performance computing systems. The host core is usually a CPU, while the accelerator could be any kind of co-processors like a GPU or an approximate accelerator. The dedicated quality management blocks controls error detection and does needed correction.

\begin{figure}[!ht]
  \begin{center}
      \includegraphics[height=180pt]{efficientFramework}
      \caption[Quality Management Framework by CURE Laboratory]{Proposed Framework by CURE Laboratory \cite{EffectiveQualityWang}}
      \label{fig:efficientFramework}
  \end{center}
\end{figure}

