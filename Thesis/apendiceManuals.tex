\chapter{Manuals}
\label{ch:appendixManuals}

\noindent
\hypertarget{link_sm01}{\Large\textbf{Guide ID: SM-01}}\\
\normalsize
\textbf{Title:} Compiling Hardware Design.\\\\
\textbf{Description:}\\
How to compile the FPGA Hardware design that is delivered as part of the GSRD
(Golden System Reference Design) release, necessary to create and generate a
preloader for booting from the SD Card on a development board such as Cyclone V.\\\\
\textbf{Prerequisites:}\\
\begin{itemize}
	\item Host PC running Linux (Kubuntu 18.04 was used)
	\item Altera Quartus II v16 or greater (v17.1 was used)
	\item GHRD v16.0 or greater (v17 was used). Available at: 
	\url{http://releases.rocketboards.org/release/2016.05/gsrd/hw}\\\\
\end{itemize}

\noindent
\textbf{Procedure:}\\

\begin{enumerate}
	\item Retrieve the GHRD release from RocketBoards website.
	\begin{lstlisting}[language=Bash,basicstyle=\small]
	$ wget https://releases.rocketboards.org/release/2016.05/gsrd/hw/cv_soc_devkit_ghrd.tar.gz\end{lstlisting}
	
	\item Extract the files from the archive and make all the files writable.
	\begin{lstlisting}[language=Bash,basicstyle=\small]
	$ tar xzf cv_soc_devkit_ghrd.tar.gz
	$ chmod +w -R ~/cv_soc_devkit_ghrd/\end{lstlisting}

	\item Start the Quartus II software.
	\item In Quartus go to:\\
		  File $\rightarrow$ Open Project. \\
		  Browse the file \textbf{\path{../cv_soc_devkit_ghrd/soc_system.qpf}.}\\
		  Click open.
	\item In Quartus go to:\\
		  Tools $\rightarrow$ Platform Designer (Old Qsys tool).
	\item Platform designer will ask to open a file.\\
		  Select the file \textbf{\path{../cv_soc_devkit_ghrd/soc_system.qsys}.}\\
		  Click open.
	\item In Platform Designer go to:\\
		  Generate $\rightarrow$ Generate HDL.\\
		  Click generate.\\
		  \underline{Note:} A message will be displayed when the generation is complete
		  (may take a few minutes depending on host machine speed)
	\item Close Platform Designer and return to Quartus main window.
	\item In the \textbf{Tasks} panel, select \textbf{Assembler}, right click it and 
		  select \textbf{Start}.\\
	\end{enumerate}

	The following files are generated at the end of the process:\\

	\begin{table}[!ht]
	\scriptsize
	\begin{tabular}{ll}
	\rowcolor[HTML]{EFEFEF} 
	{\color[HTML]{000000} File}                                                 & Description                                    \\
	-/cv\_soc\_devkit\_ghrd/output\_files/soc\_system.sof                      & SRAM Object File - for programming FPGA        \\
	-/cv\_soc\_devkit\_ghrd/soc\_system.sopcinfo                               & SOPC Info File - Used by Device Tree Generator \\
	-/cv\_soc\_devkit\_ghrd/soc\_system/synthesis/soc\_system\_hps\_0\_hps.svd & System View File - Used by ARM DS-5 AE         \\
	-/cv\_soc\_devkit\_ghrd/hps\_isw\_handoff                                  & Handoff folder - Used by Preloader Generator  
	\end{tabular}
	\end{table}
	

\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}



%################################################################################
\vspace{\baselineskip}
\noindent
\hypertarget{link_sm02}{\Large\textbf{Guide ID: SM-02}}\\
\normalsize
\textbf{Title:} Generating and Compiling the Preloader.\\\\
\textbf{Description:}\\
How to generate and compile the Preloader, based on the handoff information 
from Quartus. The preloader is the small portion of software in charge of
loading of the next boot program, such as the u-boot software or the final
application. It is a requirement when a Bare Metal app is needed to be loaded 
when the board is powered on, and then configure the FPGA inmediatly.\\\\
\textbf{Prerequisites:}\\
\begin{itemize}
	\item Completition of the \hyperlink{link_sm01}{Guide SM-01}\\
\end{itemize}

\noindent
\textbf{Procedure:}\\
\begin{enumerate}
	\item Move to the embedded directory and execute \textit{embedded\_command\_shell.sh}.\\
	\textit{INSTALL\_DIR} is usually: \path{/home/user/intelFPGA_lite/17.1}

	\begin{lstlisting}[language=Bash,basicstyle=\small]
	$ cd <INSTALL_DIR>/embedded/
	$ ./embedded_command_shell.sh\end{lstlisting}

	\item Move to the folder where GHRD was extracted on \hyperlink{link_sm01}{Guide SM-01}.
		  and execute the BSP Editor program.

	\begin{lstlisting}[language=Bash,basicstyle=\small]
	$ cd /cv_soc_devkit_ghrd
	$ bsp-editor &\end{lstlisting}

	\item In the BSP Editor:\\
		  Select: File $\rightarrow$ New BSP

	\item In the new BSP window click the \textbf{"..."} browse button to search for
		  the handoff folder. And select the:\\
		  \textbf{\path{ cv_soc_devkit_ghrd/hps_isw_handoff/soc_system_hps_0}} 
		  folder and click Open.
		  Click OK to close the window.
	
	\item In the BSP Editor window, edit any settings if necessary, then click Generate.
		  When editing the settings at least verify these options are specified
		  as follows:
	
	
	\begin{table}[!ht]
	\centering
	\small
	\begin{tabular}{lll}
	\rowcolor[HTML]{EFEFEF} 
	{\color[HTML]{000000} Option} & Description                                                                                                                                                            & Status    \\
	FAT Support                   & \begin{tabular}[c]{@{}l@{}}Will add the preloader the option to support a FAT\\  partition that will be necessary to load the u-boot or the\\ application.\end{tabular} & Checked   \\
	WATCHDOG\_ENABLE              & \begin{tabular}[c]{@{}l@{}}Must be unchecked otherwise the program will end\\ after a couple of seconds.\end{tabular}                                                   & Unchecked \\
	SERIAL\_SUPPORT               & \begin{tabular}[c]{@{}l@{}}Allows the view of the serial output via the UART\\ to USB port on the board.\end{tabular}                                                  & Checked  
	\end{tabular}
	\end{table}

	\item The following items are generated in the 
		  \path{cv_soc_devkit_ghrd/software/spl_bsp/} folder.
	
	\begin{table}[!ht]
	\centering
	\small
	\begin{tabular}{ll}
	\rowcolor[HTML]{EFEFEF} 
	{\color[HTML]{000000} File} & Description                                                                                       \\
	generated                   & Folder containing source code generated from the handoff folder \\
	settings.bsp                & Preloader settings file             \\
	Makefile                    & Makefile used to build the Preloader                                                              \\
	preloader.ds                & ARM DS-5 AE that can be used to load the Preloader                                               
	\end{tabular}
	\end{table}

	\item The final step is to compile the preloader that has been generated. Remember
		  that a embedded command shell is needed because we need the environment path
		  variable to have the arm cross compilation path.
	
	\begin{lstlisting}[language=Bash,basicstyle=\small]
		$ cd cv_soc_devkit_ghrd/software/spl_bsp 
		$ make\end{lstlisting}

	\item The make process will generate the following files:
	
	\begin{table}[!ht]
	\centering
	\normalsize
	\begin{tabular}{ll}
	\rowcolor[HTML]{EFEFEF} 
	{\color[HTML]{000000} File}      & Description                                      \\
	uboot-socfpga/spl/u-boot-spl     & Preloader ELF file.                               \\
	uboot-socfpga/spl/u-boot-spl.bin & Preloader binary file.                            \\
	preloader-mkpimage.bin           & Preloader image with the BootROM required header.
	\end{tabular}
	\end{table}
\end{enumerate}


\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}

%################################################################################
\vspace{\baselineskip}
\noindent
\hypertarget{link_sm03}{\Large\textbf{Guide ID: SM-03}}\\
\normalsize
\textbf{Title:} Creating and Updating boot SD Card.\\\\
\textbf{Description:}\\
How to generate the SD layout, load the require files and use it to boot from
the SD card on the development board succesfully.\\\\
\textbf{Prerequisites:}\\
\begin{itemize}
	\item Completition of the \hyperlink{link_sm02}{Guide SM-02}\\
\end{itemize}

\noindent
\textbf{Information:}\\



\begin{minipage}{0.34\textwidth}
	\begin{figure}[H]
		\includegraphics[width=150pt]{sdLayout}
		\caption{\label{fig:sd_layout} SD Layout Model}
	\end{figure}
\end{minipage} \hfill 
\begin{minipage}{0.64\textwidth}
As seen on figure \ref{fig:sd_layout} the SD card will need 3 partitions plus 
the MBR (Master Boot Record) and the U-Boot environment settings. This layout must
be used to achieve the correct load of the FPGA program and bare metal application.
Partition 3 is a custom partition with type = 0xA2. It is \textbf{required} by the
BootROM, which will identify it from the MBR and load the preloader from the 
beginning of it. The MBR (Master Boot Record) contains descriptions of the partitions 
on the card, including their type, location and length.\\\\
%\begin{table}[!ht]
\scriptsize
\begin{tabular}{lll}
\rowcolor[HTML]{EFEFEF} 
{\color[HTML]{000000} Location} & File name              & Description                                                                                     \\
								& u-boot.scr             & U-boot script for configuring FPGA                                                              \\
								& soc\_system.rbf        & FPGA configuration file                                                                         \\
\multirow{-3}{*}{Partition 1}   & baremetalapp.bin       & Bare metal app to be loaded to HPS                                                              \\
Partition 2                     & Variuos archives       & \begin{tabular}[c]{@{}l@{}}Linux filesystem (NOT used in the\\ baremetal approach)\end{tabular} \\
								& preloader-mkpimage.bin & Preloader image                                                                                 \\
\multirow{-2}{*}{Partition 3}   & u-boot.img              & U-Boot image                                                                                   
\end{tabular}
%\end{table}
\end{minipage}

\bigskip
\noindent
\textbf{Procedure:}\\
\noindent
The release of the Golden System Reference Design (GSRD) User Manuals includes a 
tool that can be used to create a bootable SD card image from the items mentioned 
in the table above. The tool is named\textbf{ make\_sdimage.py} and is available at:

\begin{itemize}
	\item v17.1: \url{http://releases.rocketboards.org/release/2017.10/gsrd/tools/make_sdimage.py}
	\item v18.1: \url{http://releases.rocketboards.org/release/2018.05/gsrd/tools/make_sdimage.py}
\end{itemize}

\begin{enumerate}
	\item Create a new working directory (this directory must include all the items
	mentioned in the table above). Move into this directory, download the required 
	script and create a directory for the filesystem.

	\begin{lstlisting}[language=Bash,basicstyle=\small]
		$ mkdir GenerateSD
		$ cd GenerateSD
		$ wget http://releases.rocketboards.org/release/2018.05/gsrd/tools/make_sdimage.py
		$ mkdir rootfs\end{lstlisting}

	\item More information about the script functionality can be obtained if it is
	executed with the help parameter.

	\begin{lstlisting}[language=Bash,basicstyle=\small]
		$ sudo stdbuf -i0 -o0 -e0 python make_sdimage.py -h
		usage: make_sdimage.py [-h] [-P PART_ARGS] [-s SIZE] [-n IMAGE_NAME] [-f]

		Creates an SD card image for Altera's SoCFPGA SoC's

		optional arguments:
		-h, --help     show this help message and exit
		-P PART_ARGS   specifies a partition. May be used multiple times. file[,file
						,...],num=,format=,
						size=[,type=ID]
		-s SIZE        specifies the size of the image. Units K|M|G can be used.
		-n IMAGE_NAME  specifies the name of the image.
		-f             deletes the image file if exists

		Usage: PROG [-h] -P  [-P ...] -P \end{lstlisting}

	\item Execute the script with the following parameters to generate the SD image.
	The \textbf{u-boot.img} can be placed both in partition 1 or partition 3. The
	\textbf{stdbuf -i0 -o0 -e0} command is required to show the output of the script on 
	the console, otherwise even the error messages are omited and it is not possible
	to understand what is really happening, the script might even hang forever.

	\begin{lstlisting}[language=Bash,basicstyle=\small]
		$ sudo stdbuf -i0 -o0 -e0  python make_sdimage.py \
		-f \
		-P preloader-mkpimage.bin,num=3,format=raw,size=10M,type=A2 \
		-P rootfs/*,num=2,format=ext3,size=100M \
		-P baremetalapp.bin,u-boot.scr,u-boot.img,soc_system.rbf,num=1,format=vfat,size=100M \
		-s 8G \
		-n sdcard_cycloneV.bin \
	\end{lstlisting}

	This will create sdcard\_cycloneV.bin image. If no error was shown just burn the 
	generated image with a program such as Disks or gParted and the boot process should
	work as expected.\\
	
	\item \textbf{Troubleshoot:} if the script fails when generating the Partition 1 the following
	message will appear:\\
	
	\begin{lstlisting}[language=Bash,basicstyle=\small]
		info: creating the image sdcard_cycloneV.bin
		info: creating the partition table
		info: processing partitions...
			partition #1...
		error: format: failed\end{lstlisting}
	
	This happens because the script failed when trying to generate the partition with
	FAT32 format, as we are on a linux environment. To solve this problem change the line 
	where vFat is specified from:
	\begin{lstlisting}[language=Bash,basicstyle=\small,firstnumber=5]
	-P baremetalapp.bin,u-boot.scr,u-boot.img,soc_system.rbf,num=1,format=vfat,size=100M \\end{lstlisting}
	to:\\
	\begin{lstlisting}[language=Bash,basicstyle=\small,firstnumber=5]
	-P baremetalapp.bin,u-boot.scr,u-boot.img,soc_system.rbf,num=1,format=ext4,size=100M \\end{lstlisting}

	\item Burn the generated image with a program such as Disks or gParted.
	\item Open gparted and select the SD card to inspect the partitions. Right click
		  partition 1 as shown in figure \ref{fig:format1}.

		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[width=300pt]{format1}
				\caption{Right click on partiton 1. GParted program.}
				\label{fig:format1}
			\end{center}
		\end{figure}

	\item Select \textbf{Format to $\rightarrow$} in the menu and then FAT32 as shown in 
		  figure \ref{fig:format2}.

		\begin{figure}[!ht]
			\begin{center}
				\includegraphics[width=300pt]{format2}
				\caption{Partition format selection. GParted program.}
				\label{fig:format2}
			\end{center}
		\end{figure}
	
	\item Click \textbf{Apply} icon to write the changes to the SD Card.\\\\
		  So far the SD card is formatted correctly with the three required
		  partitions, however when the filesystem of partition 1 was changed from 
		  \textbf{EXT4} to \textbf{FAT32} all the files were deleted. So it is necessary
		  to copy again all the files that were previously burn into partition 1 from
		  the \textit{sdcard\_cycloneV.bin} image generated on step 4.
	
	\item Identify partition mount points path. For example, on figure \ref{fig:format1}
		  partiton 1 is mounted on the path \path{/dev/mmcblk0p1}. Partition 2 is mounted
		  on \path{/dev/mmcblk0p2} and partiton 3 is mounted on \path{/dev/mmcblk0p3}.

	\item Mount the filesystem on the PC and copy the required files as necessary. Please
		  note \textit{{FILENAME}} refers to each file required to be in partition 1 as 
		  shown in the table next to figure \ref{fig:sd_layout}.

		\begin{lstlisting}[language=Bash,basicstyle=\small]
		$ sudo mkdir sdcard
		$ sudo mount /dev/mmcblk0p1 sdcard/
		$ sudo cp FILENAME sdcard/
		$ sudo umount sdcard\end{lstlisting}

		Now the board must boot correctly.

	\item \underline{OPTIONAL:} If for some reason the preloader image must be changed, to save the
		  time necessary to burn the SD image, the preloader file can be loaded in the 
		  following way: (Please note \textbf{of=} parameter must match the partition 3
		  mount point as seen on figure \ref{fig:format1})

		\begin{lstlisting}[language=Bash,basicstyle=\small]
		  $ sudo dd if=preloader-mkpimage.bin of=/dev/mmcblk0p3 bs=64k seek=0
		  $ sync\end{lstlisting}
\end{enumerate}

%\noindent
%\textbf{References:}\\
%The previous configuration manuals were created based on the tutorials found at %\hyperlink{rocketboards.org}{rocketboards.org}\\

%\begin{itemize}
	%\item Manual SM01: \hyperlink{https://rocketboards.org/foswiki/Documentation/GSRD160CompileHardwareDesign}{https://rocketboards.org/foswiki/Documentation/GSRD160CompileHardwareDesign}
	%\item Manual SM02: \hyperlink{https://rocketboards.org/foswiki/Documentation/GSRDPreloader}{https://rocketboards.org/foswiki/Documentation/GSRDPreloader}
	%\item Manual SM03: \hyperlink{https://rocketboards.org/foswiki/Documentation/GSRDSdCard}{https://rocketboards.org/foswiki/Documentation/GSRDSdCard}
%\end{itemize}

%\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}