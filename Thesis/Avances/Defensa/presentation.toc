\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{Contexto}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Problema}{5}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{Objetivos}{6}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Revisi\IeC {\'o}n te\IeC {\'o}rica}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Conceptos importantes}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Enfoques actuales}{9}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{M\IeC {\'e}todos y soluci\IeC {\'o}n}{11}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Arquitectura propuesta}{11}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Interfaz de tarjeta SD}{12}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{N\IeC {\'u}cleo exacto y de control}{13}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{4}{N\IeC {\'u}cleo aproximado}{14}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{5}{Proceso de entrenamiento}{15}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Resultados}{16}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Entrenamiento Modelo Lineal}{16}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{M\IeC {\'e}tricas}{17}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{3}{Comparaci\IeC {\'o}n de Modelos}{18}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{4}{Comparaci\IeC {\'o}n de Periodicidad}{19}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Conclusiones}{20}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{1}{Conclusiones}{20}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{2}{Recomendaciones}{21}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{3}{Trabajo futuro}{22}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Resumen}{23}{0}{6}
