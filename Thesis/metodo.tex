\chapter{Methodological Framework}
\label{ch:metodo}

\section{Development Methodology}

Due to the investigative nature of this project, which also encompasses an exploration phase around the state-of-the-art to learn about how the problems are being handled now and how to use the strengths of each method to propose a new model, the work methodology chose was the \textit{Spiral Model}.

Some of the advantages of working under the spiral methodology are: additional functionality or changes can be done at a later stage; this is useful, as the project is highly investigative some scopes or objectives might be changed, continous or repeated development helps in risk management; also helpful as it is necessary to explore different options on how to handle events of the platform that is being developed, development is fast and features are added in a systematic way; due to the limited time for the project the process should be done as fast as possible. Other advantages include: cost estimation becomes easy as the prototype building is done in small fractions and there is always space for customer feedback; which in this case is the important input by project's supervisors \cite{SpiralBook} .

Figure \ref{fig:spiralMethod} represents the spiral method, each color represents a different step of the project development. On the research process, all of the state-of-the-art works are reviewed, summarized and arranged. The next step refers to the Concept Development, where some ideas will be tested to identify how they work according to what is expected and how the can be merged or reused. The Main Development focuses on how the proposal of the new model to control the error on run time while using approximate accelerators. Finally, the System Enhancement step takes care of improving the system's performance and compare the results obtained and how they can be compared to the current research focus of other authors.

\begin{figure}[!ht]
    \begin{center}
        \includegraphics[height=190pt]{spiralMethod}
        \caption[Iterative Training Distribution]{Spiral Method Representation}
        \label{fig:spiralMethod}
    \end{center}
\end{figure}

\section{Development Tools}
The nature of the project involves both hardware and software scopes, in which different development tools are needed to accomplish the objectives. Table \ref{table:usedTools} summarizes the ones that are most important and how they are useful for the project.

\begin{table}[!ht]
        \begin{center}
        \caption{Hardware and software tools for the project}
        \tiny
        \begin{tabular}{llll}
            Tool               & Version          & Description                                                                                                    & Use                                                                                                                       \\ \hline
            Cyclone V          & DE1 SoC          & \begin{tabular}[c]{@{}l@{}}Altera Development Board that includes \\ a FPGA and a HPS integrated.\end{tabular} & \begin{tabular}[c]{@{}l@{}}Implement the designed system and test\\ its functionality on a hardware board.\end{tabular}   \\
            Quartus Prime Lite & 17.1.0 Build 590 & Intel FPGA development studio                                                                                  & \begin{tabular}[c]{@{}l@{}}Program and design the system architecture\\ for the Cyclone.\end{tabular}                     \\
            DS-5 Dev. Studio   & 5.29.1           & ARM development studio                                                                                         & Compile the ARM code for the HPS.                                                                                         \\
            Visual Studio Code & 1.32.3           & Microsoft Lite Text Editor                                                                                     & \begin{tabular}[c]{@{}l@{}}Write and test most of the source code\\ as well as the latex documentation code.\end{tabular} \\
            Golden System RD   & 2016.05          & \begin{tabular}[c]{@{}l@{}}Design reference for the Cyclone V\\ board.\end{tabular}                            & \begin{tabular}[c]{@{}l@{}}Use as a test tool to identify all of the \\ available hardware components.\end{tabular}      
            \end{tabular}
        \vspace{5pt}
        
        \label{table:usedTools}
    \end{center}
\end{table}    

The \textit{Platform Designer} tool, which is included in the Quartus Prime software, previously known as \textit{Qsys} provides different hardware modules that accelerates the development process, for example: the AXI Bridges HPS-to-FPGA that allow the exchange of information between the two processing units in an efficient way. On the other side, a hardware module for the DMA (Direct Memory Access) is also provided by the Altera libraries on the \textit{Platform Designer} environment. The connection between most of the pregenerated modules is easily made by using a GUI-based connection system. The ARM development studio provides the necessary compilation tools to generate the cross-compile C code that will be able to run under the HPS (Arm Cortex A9 processor). However, to compile the code is necessary to \textit{source} the environmental variables and then perform the execution of the \textit{make} process.

\section{Validation Strategy}
The software applications that will be tested on the designed system are all based on images filters as mentioned before (LaPlace, Gauss, Sobel). To validate the performance and efficiency, a set of test images (as seen on figure \ref{fig:testImages}) will be used by apply the filters to them. To compare the results several metrics will be recorded for each execution of the program with the corresponding image. However to simplify the load process from memory, they will only be used in a single channel mode (black and white version of the images).

\begin{figure}[!ht]
    \begin{center}
        \includegraphics[width=300pt]{testImages}
        \caption[Test images for system validation]{Common test images used in the image processing area}
        \label{fig:testImages}
    \end{center}
\end{figure}

The PSNR (Peak signal-to-noise ratio), is a metric for the ratio between the maximum possible power of a defined signal and the power of noise that could affect the fidelity of its representation \cite{CompressionBook}. Most of the signals have a large range, therefore it is sometimes expressed in logarithmic decibel scale. The PSNR is mainly composed by the MSE (Main Squared Error), which is defined by:

\begin{equation}
    MSE = \frac{1}{mn}\sum_{i=0}^{m-1}\sum_{j=0}^{n-1}[I(i,j)- K(i,j)]^{2}
    \label{equ:MSE}
\end{equation}

Therefore the PSNR metric is defined by:

\begin{equation}
    PSNR = 20\cdot log_{10}(MAX_{I}) - 10\cdot log_{10}(MSE)
    \label{equ:PSNR}
\end{equation}


The structural similarity (SSIM) index is a method for predicting the perceived quality of digital television and cinematic pictures, as well as other kinds of digital images and videos. The early version of the model was developed in the Laboratory for Image and Video Engineering (LIVE) at The University of Texas at Austin and the full version was further developed jointly with the Laboratory for Computational Vision (LCV) at New York University. Further variants of the model were developed in the Image and Visual Computing Laboratory at University of Waterloo and have been commercially marketed.

Also, the SSIM (Structural similarity), corresponds to a method for predicting the perceived quality of digital images \cite{CompressionBook}. It is used for measuring the similarity between two images. This index is a full reference metric, i.e the measurement is based on an initial uncompressed or distortion-free image. Due to this fact, it will be helpful to compare the similarity of the images generated with an approximate accelerator to the exact original version. The SSIM is defined by:

\begin{equation}
    SSIM(x,y) = \frac{(2\mu_{x}\mu_{y}+c_{1})(2\sigma_{xy}+c_{2})}{(\mu_{x}^2+\mu_{y}^2+c_{1})(\sigma_{x}^2+\sigma_{y}^2+c_{2})}
    \label{equ:SSIM}
\end{equation}

Where,

\begin{compactitem}
    \item $\mu_{x}$ is the average of $x$.
    \item $\mu_{y}$ is the average of $y$.
    \item $\sigma_{x}^2$ is the variance of $x$.
    \item $\sigma_{y}^2$ is the variance of $y$.
    \item $\sigma_{xy}$ is the covariance of $x$ and $y$.
    \item $c_{1}=(k_{1}L)^2$ and $c_{2}=(k_{2}L)^2$ are two variables to stabilize the division denominator.
    \item $L$ is the dynamic range of pixel-values.
    \item $k_{1}$ and $k_{2}$ are constants previously defined according to the definition of the problem.
\end{compactitem}

In practice, it is common to require a single overall quality measure of an entire image, not just single pixel comparations. For this reason, the mean SSIM (MSSIM) index is used to evaluate the overall image quality \cite{ImageQualityPaper}, and it is defined by:

\begin{equation}
    MSSIM(X,Y)=\frac{1}{M}\sum_{j=1}^{M}SSIM(x_{j},y_{j})
    \label{equ:MSSIM}
\end{equation}

Regarding the machine learning training methods that will be used for the predictor, the precision is the fraction of the relevant instances among the retrieved instances \cite{PatternRecogBook}. It is defined by :

\begin{equation}
    P = \frac{TruePositives}{TruePositives + False Positives} 
    \label{equ:precision}
\end{equation}

The recall is the fraction of relevant instances that have been retrieved over the total amount of instances \cite{PatternRecogBook}. The recall is defined by: 

\begin{equation}
    R = \frac{TruePositives}{TruePositives + False Negatives} 
    \label{equ:precision}
\end{equation}