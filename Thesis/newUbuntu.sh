#!/bin/bash

if test `whoami` != "root" 
then
  echo "  Error: You need root priviledges to run this script."
  exit $FAILURE
fi


apt-get install texlive-font-utils
