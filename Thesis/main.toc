\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Figures}{iii}{chapter*.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Tables}{v}{chapter*.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}General description}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}General Background}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Company Description}{1}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Similar works}{2}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Problem Statement}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Problem Context}{2}{subsection.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Problem Description}{3}{subsection.1.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Problem Definition}{3}{subsection.1.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Objectives}{4}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.1}General Objective}{4}{subsection.1.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.2}Specific Objectives}{4}{subsection.1.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Scope, deliverables and limitations of the project}{4}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5.1}Scope}{4}{subsection.1.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5.2}Deliverables}{5}{subsection.1.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5.3}Limitations}{5}{subsection.1.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Background and Related Work}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Approximate Computing}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Definitions}{6}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Periodic Quality Control}{8}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Continous Quality Control}{11}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Methodological Framework}{16}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Development Methodology}{16}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Development Tools}{17}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Validation Strategy}{18}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Development of an Accuracy Control Architecture for Approximate Accelerators}{20}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Design and development}{20}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}SD Card Interface}{21}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Hard Processor System (Exact and Control Core)}{23}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}FPGA (Approximate Core)}{27}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Communication between HPS and FPGA}{30}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Training Process of the Hardware Prediction Module}{31}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Results and analysis}{32}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusions and recommendations}{41}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Conclusions}{41}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Recommendations}{42}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Future work}{42}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{References}{43}{chapter*.35}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Troubleshoot Guide}{46}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Manuals}{52}{appendix.B}
