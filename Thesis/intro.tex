%% ---------------------------------------------------------------------------
%% intro.tex
%%
%% Introduction
%%
%% $Id: intro.tex 1477 2010-07-28 21:34:43Z palvarado $
%% ---------------------------------------------------------------------------

\chapter{Introduction}
\label{chp:intro}

\section{General description}
This document presents the description and developed work on how to implement a system that contains two separate control blocks, a host core and an approximate accelerator, to improve the performance of error-tolerant applications. The system optimizes the error control during run-time by searching for an intermediate point between periodic and continous checks through all the program execution. In chapter \ref{ch:marco} the main teorical fundaments are explained and the state-of-the-art is reviewed, which will be necessary to explain in chapter \ref{ch:solucion} the proposed system. Chapter \ref{ch:metodo} presents the development methodology used, the required tools and the validation strategy. Also in chapter \ref{ch:solucion} an exhaustive result analysis is made to identify the main advantages of the methods developed. And finally, chapter \ref{ch:conclusiones} will tackle the conclusions, recommendations and future work in the area. Chapters further than \ref{ch:appendixTroubleshoot} are dedicated to appendices that complement the understanding of the document.

\section{General Background}

\subsection{Company Description}
The project was developed on the \textit{Chair for Embedded Systems} at the \textit{Karlsruhe Institute of Technology}. The chair is devoted to Research in Design and Architectures for Embedded Systems. A current focus of research are multi-core systems, dependability and low power design. The Chair has currently the following research groups \cite{CESRef}: 
\begin{compactitem}
    \item Security for Cyber-physical Systems
    \item Dependable Hardware
    \item Low Power and Dependability
    \item Adaptive and Self-Organizing On-Chip Systems
    \item Internet of Things
\end{compactitem}

\subsection{Similar works}
Previously, other students from the Costa Rica Institute of Technology have also developed their graduation projects on the Chair for Embedded Systems at the Karlsruhe Institute of Technology. Pablo Osorio presented his project: \textit{Development of a multi-core and multi-accelerator platform for approximate computing} on the second semester of 2017.
His work was focused on the design of a multicore and multi-accelerator platform
design that uses both exact and approximate versions, also providing interaction with a
software counterpart to ensure usage of both layouts. Using an instance of the Altera’s NIOS II processor, this work presented a comparison on how the execution through hardware approximate accelerators demonstrate a definitively improvement while using approximate accelerators in comparison with software and exact accelerator implementations. Memory accessing and filter operations times, for different matrix sizes, presented a gain in cycles measured for error tolerant applications \cite{ThesisPabloOsorio}. 



\section{Problem Statement}

\subsection{Problem Context}
Nowadays, computers have become an useful tool to simplify most common activities,
from money transactions on banks, writing documents, to replacing old way of sending
letters and changing it to simply send an email, and even to the point of autonomous
driving vehicles that require little, or even none, human interaction. Also, they
perform the computation of complex math operations that help in areas such as 
pattern recognition algorithms and machine learning activities. Applications of
these type are possible thanks to the enourmous quantity of computing resources that are now available, such as greater memory capacity, higher processor frecuencies, and efficient architectures designed to increase the performance; all of these specifications were just dreams of the computer scientists a couple of decades before. Consequently, a great number of applications, usually named recognition, mining and synthesis (RMS), have appeared and they represent a significant part of the world computing spectrum, from mobile devices and Internet of Things to big data centers \cite{AproxSurvey}.

However, reducing power consumption and circuit area, allowing the same processing of a high amount of data, are current design challenges. These needs have boosted the emergence of different design paradigms, one of them is \textit{Approximate Computing}. Approximate Computing aims to exploit error-tolerant applications to reduce the required computational effort, achieving good-enough results for the applications purposes while allowing errors in their computations. For instance, using different approximate computing techniques could allow to perform operations in a faster way, by intentionally relaxing the numerical representation, such in the case of floating-point operations. In many cases, those errors are imperceptible to the final user. 

The development of this paradigm has created a new research focus on the control
of the relationship between performance and power consumption without affecting
negatively the applications's results. Nevertheless, a big challenge is the quality
control on runtime. How to ensure that the quality of output data fulfills a previously established level?

Currently, two main scenarios have been proposed to handle the error control
on approximated accelerators at run time. The first approach focuses on doing
a periodic check, namely after a defined number of iterations a verification 
is made, but perhaps in some iterations the error could be triggered, and because
of the periodicity of the method that error might be hided.

On the other hand, the second approach handles the error in a different way. It 
performs a continous analysis, being more intrusive, but with the warranty of
a better control over the output data quality. Both methods can use a 
hardware level technique where less precise circuits are used to lower the required
voltage level and improve the power consumption. A software level technique is
also possible to use, where some calculations or memory access operations
that are not critical are skipped and the required quality level is still obtained.

\subsection{Problem Description}

The existance of these two methods illustrate the need of multiple ways to handle
the quality control on approximate architectures, which provides certain guarantees to obtain a defined accuracy level for results on an approximate environment. This work is focused on proposing a new method and architecture to ensure the quality control in a dynamic environment, searching for the balance between periodic revisions over the error that is being generated by the Approximate Accelerator, and the error prediction to better take advantage of savings to the approximations.

\subsection{Problem Definition}

Figure \ref{fig:basic_model} illustrates the main components of the proposed approach. The host core exchange require data to the Approximate Accelerator and handles the error recovery and control. The Approximate Accelerator improves the system's performance and is connected to the error prediction module. Both items have direct access to main memory which helps on the exchange of raw data, for example an image or video. Also, a data/instruction bus between both blocks facilitates the instructions exchange to perform certain routines, such as error verification, error prediction or a recompute operation. The set of host core and Approximate Accelerator will attack the main problem: How to ensure the quality stays under a predefined level while using Approximate Accelerators by the complementary use of continous and periodic checks for error?

\begin{figure}[!ht]
    \begin{center}
        \includegraphics[width=300pt]{basic_model}
        \caption[Simplified block diagram of the system]{Simplified block diagram of the system}
        \label{fig:basic_model}
    \end{center}
\end{figure}

\section{Objectives}

\subsection{General Objective}
Design a methodology of accuracy control on run time when using approximate accelerators, that allows a dynamic scheme from the range of periodic revision by software to continous revision by hardware.

\subsection{Specific Objectives}
It will be achieved by designing a system able to instance a hardware accelerator
with a quality control mecanism that searchs for the intermediate point between periodic and continous revisions. This will also guide to evaluate the efficiency of the designed mecanism. But also the development of the interaction between the host core and a reconfigurable platform subsystems will impact on the performance and data flow through each module over the architecture. On the other hand, a custom memory map will be created to optimize data transfer between the host core and reconfigurable platform systems.
Another goal corresponds to validation of the designed methodology for a group of
\textit{benchmarks} of fault-tolerant applications, such as image filters as Sobel, Gauss and LaPlace. Finally, a comparison between the proposed methodology and the methods reviewed on the state-of-the-art will synthetize the obtained results.

\section{Scope, deliverables and limitations of the project}

\subsection{Scope}
Develop a system that can execute programs for at least three error-tolerant applications such as image filters, using the C programming language for the software implementation and Verilog as the Hardware Description Language for the development of hardware modules.

\subsection{Deliverables}
\begin{itemize}
    \item Source code of the developed system with internal documentation.
    \item Guide on how to operate the system and the installations guides for any other software required to use the project.
    \item Technical documentation that will help future researchers to use the generated tools.
\end{itemize}

\subsection{Limitations}
\begin{itemize}
    \item Use of the Cyclone V DE1-SoC board to develop the project.
    \item Development time is restricted to 13 weeks.
    \item The host core must be programmed in a \textit{bare metal} approach, to ensure the maximum gain in performance. 
\end{itemize}
 
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
